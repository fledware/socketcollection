package fledware.socketcollection

import fledware.socketcollection.internal.currentTime


/**
 * the general states of a single socket
 */
enum class SocketState {
  // the connection is not completely setup, or is lost and trying
  // to reconnect.
  SHAKING,
  // the connection is working and considered open and active
  NORMAL,
  // the connection is down. this means that it was in a SHAKING
  // state for a configured amount of time.
  DOWN,
  // the connection is by the remote side when forced reconnection
  // is set. so instead of going to CLOSED, it goes to DOWN_FINDING
  // to show that the connect was closed in an expected way, but
  // it is actively trying to reconnect.
  DOWN_FINDING,
  // the connection is actually closed. you will not be able
  // to reuse the socket and it should be cleaned from collections
  CLOSED
}

/**
 * some situations may require this information, and
 * theres not reason to hide it.
 */
enum class SocketType {
  INBOUND,
  OUTBOUND
}

/**
 * general information about the socket
 */
data class SocketInfo(val host: String?,
                      val port: Int?,
                      val type: SocketType) {

  val created_at = currentTime()
}

/**
 * the main interface for sending messages.
 */
interface Socket : Tellable, SimpleService {

  val identity: String
  val info: SocketInfo
  val config: SocketCollectionConfig
  val data: MutableMap<String, Any>
  val socket_state: SocketState
  fun waitForState(state: SocketState = SocketState.NORMAL): Promise<Unit>

  fun tell(message: Any,
           total_timeout: Long = config.tell_total_timeout,
           acknowledged: Boolean = config.tell_acknowledged,
           ack_retry_timeout: Long = config.tell_ack_retry_timeout)
      : Promise<Unit>

  fun <T> ask(message: Any,
              total_timeout: Long = config.ask_total_timeout,
              retry_timeout: Long = config.ask_retry_timeout)
      : Promise<T>
}
