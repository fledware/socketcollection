package fledware.socketcollection

import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.internal.ClientSocketHandler
import fledware.socketcollection.internal.SocketCollectionImpl
import fledware.socketcollection.internal.SocketImpl
import org.slf4j.Marker


object factory {
  val standalone_config = SocketCollectionConfig(
      socket_message_buffer = 1000,
      socket_force_reconnect = true
  )

  val collection_simple_config = SocketCollectionConfig(
      socket_message_buffer = 100,
      socket_force_reconnect = false,
      collection_message_buffer = 1000
  )

  val collection_adhoc_config = SocketCollectionConfig(
      socket_message_buffer = 1000,
      socket_force_reconnect = false,
      collection_message_buffer = -1
  )

  val collection_nodes_config = SocketCollectionConfig(
      socket_message_buffer = 1000,
      socket_force_reconnect = true,
      collection_message_buffer = -1,
      collection_remove_on_down = false
  )

  /**
   * creates a socket that is connected to the given host/port. it can
   * be configured to automatically reconnect. this is a helpful way
   * to use a socket if you need to connect to a single place or multiple
   * places that are all very well known.
   * for instance, if you need to connect to a master node and all
   * other connections in the service are children, it would be helpful
   * to make a collection for the children, then a single node for the master.
   */
  fun socketCreateStandalone(host: String,
                             port: Int,
                             identity: String = identities.generate(),
                             marker: Marker? = null,
                             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
                             config: SocketCollectionConfig = standalone_config,
                             builder: ChannelInitFactory = ChannelInitFactoryDefault(),
                             socket_state: (Socket) -> Unit = { },
                             handler: InboundHandler = InboundHandlerDefault.instance): Socket {
    val info = SocketInfo(host, port, SocketType.OUTBOUND)
    val socket_handler = ClientSocketHandler(host, port, marker, identity, builder)
    val result = SocketImpl(identity, config, info, socket_state, marker, socket_handler, handler)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }

  /**
   * configured to be a simple routed/broadcast server. generally, this
   * will be connected to and the server will send messages to the clients.
   */
  fun collectionAsServerRouter(port: Int,
                               message_buffer_size: Int = 10_000,
                               name: String = identities.generate(),
                               marker: Marker? = null,
                               start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
                               builder: ChannelInitFactory = ChannelInitFactoryDefault(),
                               socket_state: (Socket) -> Unit = { },
                               handler: InboundHandler = InboundHandlerDefault.instance): SocketCollection {
    val config = SocketCollectionConfig(
        socket_message_buffer = -1,
        socket_force_reconnect = false,
        collection_message_buffer = message_buffer_size
    )
    val result = SocketCollectionImpl(name, config, marker, builder, socket_state, handler)
    result.bind(port)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }

  /**
   * general server. use this when sending messages will not happen
   */
  fun collectionAsServer(port: Int,
                         name: String = identities.generate(),
                         marker: Marker? = null,
                         config: SocketCollectionConfig = SocketCollectionConfig(
                             socket_message_buffer = -1,
                             socket_force_reconnect = false,
                             collection_message_buffer = -1
                         ),
                         start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
                         builder: ChannelInitFactory = ChannelInitFactoryDefault(),
                         socket_state: (Socket) -> Unit = { },
                         handler: InboundHandler): SocketCollection {
    val result = SocketCollectionImpl(name, config, marker, builder, socket_state, handler)
    result.bind(port)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }

  /**
   * configured to be a simple routed/broadcast client. generally, this
   * will connect to servers and then send messages to the servers
   */
  fun collectionAsClientRouter(message_buffer_size: Int = 10_000,
                               name: String = identities.generate(),
                               marker: Marker? = null,
                               start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
                               builder: ChannelInitFactory = ChannelInitFactoryDefault(),
                               socket_state: (Socket) -> Unit = { },
                               handler: InboundHandler = InboundHandlerDefault.instance): SocketCollection {
    val config = SocketCollectionConfig(
        socket_message_buffer = -1,
        socket_force_reconnect = false,
        collection_message_buffer = message_buffer_size
    )
    val result = SocketCollectionImpl(name, config, marker, builder, socket_state, handler)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }

  /**
   * general purpose collection.
   */
  fun collectionAsConfigured(name: String = identities.generate(),
                             marker: Marker? = null,
                             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
                             config: SocketCollectionConfig = collection_simple_config,
                             builder: ChannelInitFactory = ChannelInitFactoryDefault(),
                             socket_state: (Socket) -> Unit = { },
                             handler: InboundHandler = InboundHandlerDefault.instance): SocketCollection {
    val result = SocketCollectionImpl(name, config, marker, builder, socket_state, handler)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
}

