package fledware.socketcollection.shard

import fledware.socketcollection.Promise
import fledware.socketcollection.SerialInboundHandler
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.SimpleServiceState
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.SocketState
import fledware.socketcollection.extend
import fledware.socketcollection.factory
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.internal.currentTime
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.concurrent.ConcurrentHashMap


internal class ShardManager(val shard_id: String,
                            factory: (String) -> Shard) {
  val shard: Shard = factory(shard_id)
  val receiver = SerialInboundHandler { s, m, i ->
    try {
      return@SerialInboundHandler shard.receive(s, m, i)
    }
    catch (ex: Exception) {
      return@SerialInboundHandler ex
    }
  }

  suspend fun start() {
    shard.start()
  }

  suspend fun shutdown() {
    receiver.close()
    shard.close()
  }
}

class ShardNode(private val config: ShardConfiguration,
                private val self_host: String,
                private val self_port: Int,
                private val shard_creater: (String) -> Shard,
                private val kill: Promise<Unit>,
                val marker: Marker?,
                builder: ChannelInitFactory = ChannelInitFactoryDefault()) : SimpleServiceImpl(marker) {
  companion object {
    private val logger = LoggerFactory.getLogger(ShardNode::class.java)
    private val RUNNING = setOf(SimpleServiceState.STARTING, SimpleServiceState.RUNNING)
    private val SOCKET_UP = setOf(SocketState.SHAKING, SocketState.NORMAL)
  }

  val identifier = "${config.shard_name}-$self_host-$self_port"
  private val shards = ConcurrentHashMap<String, ShardManager>()
  val shard_ids: Set<String> get() = shards.keys

  private val world_collection = factory.collectionAsServer(self_port,
                                                            name = identifier,
                                                            marker = marker.extend("world"),
                                                            start_strategy = SimpleServiceStart.LAZY,
                                                            builder = builder,
                                                            handler = { s, m, i -> worldHandleInbound(s, m, i) })

  private val shard_master = factory.socketCreateStandalone(config.master_bind_host,
                                                            config.master_bind_shard,
                                                            config = SocketCollectionConfig(
                                                                socket_message_buffer = 1000,
                                                                socket_force_reconnect = true,
                                                                tell_acknowledged = true
                                                            ),
                                                            marker = marker.extend("master"),
                                                            identity = identifier,
                                                            start_strategy = SimpleServiceStart.LAZY,
                                                            builder = builder,
                                                            socket_state = this::masterSocketStateChange,
                                                            handler = { s, m, i -> masterHandleInbound(s, m, i) })

  private val metric_sender = launch(CommonPool, start = CoroutineStart.LAZY) {
    while (isServiceState(RUNNING)) {
      delay(config.metric_delay_millis)
      if (shard_master.socket_state != SocketState.NORMAL) {
        continue
      }
      try {
        val metrics = shards.mapValues { it.value.shard.metric }
        shard_master.tell(ShardProtocol.WorkerMetrics(identifier, metrics))
      }
      catch (ex: Exception) {
        logger.warn(marker, "error while generating metrics", ex)
      }
    }
  }

  override fun actualStart(): Promise<Unit> {
    return future {
      logger.warn(marker, "starting")
      shard_master.start().await()
      world_collection.start().await()
      metric_sender.start()
      Unit
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    return future {
      metric_sender.cancel()
      metric_sender.join()

      shard_master.tell(ShardProtocol.WorkerDrain(identifier))
      val start = currentTime()
      while (true) {
        if (shards.isEmpty()) break
        if (shard_master.socket_state !in SOCKET_UP) break
        if (start + config.leaving_wait_millis > currentTime()) {
          logger.warn(marker, "timeout while waiting for master to empty shards")
          break
        }
        delay(250)
      }

      shard_master.shutdown().await()
      world_collection.shutdown().await()

      // if we still have shards, then close them
      for (shard in shards.values) {
        try {
          shard.shutdown()
        }
        catch (ex: Exception) {
          logger.warn(marker, "error while shard shutdown: ${shard.shard_id}", ex)
        }
      }
      shards.clear()

      Unit
    }
  }

  // ======================================================
  // world endpoints
  // ======================================================

  @Suppress("UNUSED_PARAMETER")
  suspend fun worldHandleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    when (message) {
      is ShardProtocol.ShardMessage -> {
        val manager = shards[message.shard_id]
        if (manager == null) return ShardNotFoundException(message.shard_id, true)
        return manager.receiver.handler(socket, message.message, is_ask)
      }
      else                          -> {
        logger.warn(marker, "unhandled message: $message")
        return IllegalArgumentException("unhandled message")
      }
    }
  }

  // ======================================================
  // shard endpoints
  // ======================================================

  private fun masterSocketStateChange(socket: Socket) {
    when (socket.socket_state) {
      SocketState.SHAKING      -> {
        logger.warn(marker, "server connection is down")
      }
      SocketState.NORMAL       -> {
        logger.warn(marker, "server back up")
        if (isServiceState(RUNNING)) {
          socket.tell(ShardProtocol.WorkerJoining(identifier, 100, self_host, self_port, shards.keys.toSet()))
        }
        else {
          shard_master.tell(ShardProtocol.WorkerDrain(identifier))
        }
      }
      SocketState.CLOSED       -> {
        logger.error(marker, "server is going away... come back soon!")
      }
      SocketState.DOWN         -> {
        kill.completeExceptionally(IllegalStateException("master is down or thinks self is down"))
      }
      SocketState.DOWN_FINDING -> {
        logger.warn(marker, "server left in an expected way... come back soon!")
      }
    }
  }

  @Suppress("UNUSED_PARAMETER")
  suspend fun masterHandleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    when (message) {
      is ShardProtocol.ShardStart -> {
        if (isServiceState(RUNNING)) {
          shards.computeIfAbsent(message.shard_id) { ShardManager(message.shard_id, shard_creater) }
          logger.info(marker, "shard successfully started: ${message.shard_id}")
          return true
        }
        else {
          logger.info(marker, "shard cannot be added to node: ${message.shard_id}")
          return false
        }
      }
      is ShardProtocol.ShardStop  -> {
        val shard = shards.remove(message.shard_id)
        if (shard != null) {
          shard.shutdown()
          logger.info(marker, "shard removed from node: ${message.shard_id}")
        }
        else {
          logger.info(marker, "shard doesnt exist to remove: ${message.shard_id}")
        }
        return true
      }
      else                        -> {
        logger.warn(marker, "unhandled message: $message")
        return IllegalArgumentException("unhandled message")
      }
    }
  }

}
