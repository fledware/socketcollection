package fledware.socketcollection.shard

import fledware.socketcollection.MessageTimeoutException
import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.extend
import fledware.socketcollection.factory
import fledware.socketcollection.identities
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.internal.SocketSendAsk
import fledware.socketcollection.pubsub.PubSubClient
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.concurrent.ConcurrentHashMap


class ShardClient(private val config: ShardConfiguration,
                  private val pubsub: PubSubClient,
                  private val marker: Marker?,
                  builder: ChannelInitFactory = ChannelInitFactoryDefault()) : SimpleServiceImpl(marker) {
  companion object {
    private val logger = LoggerFactory.getLogger(ShardClient::class.java)
    private val INACTIVE = "inactive"
    private fun shardEvents(shard_name: String) = setOf(ShardEvents.topicShard(shard_name))
  }

  private val identifier = identities.generate()
  private val shards = ConcurrentHashMap<String, String>()
  private fun shardWorkerIdentifier(worker_id: String) = "$identifier-$worker_id"

  private val master = factory.socketCreateStandalone(config.master_bind_host,
                                                      config.master_bind_world,
                                                      marker = marker.extend("master"),
                                                      identity = "$identifier-master",
                                                      start_strategy = SimpleServiceStart.LAZY,
                                                      socket_state = this::masterSocketState,
                                                      config = SocketCollectionConfig(
                                                          socket_message_buffer = 1_000,
                                                          socket_force_reconnect = true,
                                                          socket_allow_send_retry = true,
                                                          socket_down_after_millis = 10_000,
                                                          socket_close_timeout = 2_000,
                                                          ask_total_timeout = 8000L,
                                                          ask_retry_timeout = 2200L
                                                      ),
                                                      builder = builder)

  private val shard_collection = factory.collectionAsConfigured(name = "${config.shard_name}-client-$identifier",
                                                                marker = marker.extend("shards"),
                                                                start_strategy = SimpleServiceStart.LAZY,
                                                                config = factory.collection_nodes_config,
                                                                socket_state = this::socketStateChange,
                                                                builder = builder)

  override fun actualStart(): Promise<Unit> {
    return future {
      pubsub.subscribe(shardEvents(config.shard_name), this@ShardClient::shardEvent).await()
      master.start().await()
      shard_collection.start().await()
      Unit
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    return future {
      pubsub.unsubscribe(shardEvents(config.shard_name), this@ShardClient::shardEvent).await()
      master.shutdown().await()
      shard_collection.shutdown().await()
      Unit
    }
  }

  private fun shardEvent(@Suppress("UNUSED_PARAMETER") topics: Set<String>, event: Any) {
    when (event) {
      is ShardEvents.NodeJoin     -> {
        val node_id = shardWorkerIdentifier(event.node_identity)
        val socket = shard_collection.sockets[node_id]
        if (socket == null) {
          shard_collection.connect(event.node_host, event.node_port, node_id)
        }
        else if (socket.info.host != event.node_host || socket.info.port != event.node_port) {
          shard_collection.disconnect(node_id)
          shard_collection.connect(event.node_host, event.node_port, node_id)
        }
      }
      is ShardEvents.NodeLeft     -> {
        val node_id = shardWorkerIdentifier(event.node_identity)
        shard_collection.disconnect(node_id)
      }
      is ShardEvents.ShardDeleted -> {
        shards.remove(event.shard_id)
      }
      is ShardEvents.ShardRemoved -> {
        shards.put(event.shard_id, INACTIVE)
      }
      is ShardEvents.ShardStarted -> {
        shards.put(event.shard_id, shardWorkerIdentifier(event.node_identity))
      }
    }
  }

  private fun masterSocketState(socket: Socket) {
    logger.info(marker, "master connection state changed: ${socket.socket_state}")
  }

  private fun socketStateChange(socket: Socket) {
    logger.info(marker, "node connection state changed: ${socket.identity} -> ${socket.socket_state}")
  }

  
  fun shardEnsure(shard_id: String): Promise<Unit> {
    return future {
      val node = master.ask<WorkerNode>(ShardProtocol.ShardEnsure(shard_id)).await()
      logger.debug(marker, "ensured shard $shard_id on $node")
      connectToNodeAndEnsure(node)
      Unit
    }
  }
  
  fun shardDestroy(shard_id: String): Promise<Unit> {
    return master.ask(ShardProtocol.ShardDestroy(shard_id))
  }

  fun <T> ask(shard_id: String, message: Any): Promise<T> {
    val result = Promise<T>()
    @Suppress("UNCHECKED_CAST")
    if (!tell(shard_id, SocketSendAsk(message, result as Promise<Any>))) {
      result.completeExceptionally(IllegalStateException("unable to send message"))
    }
    return result
  }

  fun tell(shard_id: String, message: Any): Boolean {
    val actual_message = when (message) {
      is SocketSendAsk -> ShardProtocol.ShardMessage(shard_id, message.ask, true)
      else             -> ShardProtocol.ShardMessage(shard_id, message, false)
    }
    return when (message) {
      is SocketSendAsk -> tellAttempt(actual_message, message.promise)
      else             -> tellAttempt(actual_message, null)
    }
  }

  private fun tellAttempt(message: ShardProtocol.ShardMessage, ask: Promise<Any>?): Boolean {
    if (ask?.isDone == true) {
      // timeouts or something like that happened
      return false
    }
    future {
      var attempt = 0
      while (true) {
        if (attempt >= 5) throw IllegalStateException("max retries reached")
        if (ask?.isDone == true) throw IllegalStateException("completed but message never sent")
        if (attempt > 0) delay(100L)
        attempt++

        var node_id = shards[message.shard_id]
        if (node_id == null || node_id == INACTIVE || attempt > 0) {
          // we need to try to find the node
          logger.debug(marker, "looking for node for shard: ${message.shard_id} -> $node_id")
          val node = master.ask<WorkerNode>(ShardProtocol.GetShardNode(message.shard_id)).await()
          logger.debug(marker, "found shard ${message.shard_id} on node: $node")
          node_id = connectToNodeAndEnsure(node).identity
//          node_id = shardWorkerIdentifier(node.identifier)
//          logger.debug(marker, "found shard ${message.shard_id} on node: $node")
//          shards[message.shard_id] = node_id
//          val socket = try {
//            shard_collection.sockets[node_id] ?: shard_collection.connect(node.host, node.port, node_id)
//          }
//          catch (ex: IllegalStateException) {
//            logger.info("already attempting to connect to $node_id")
//            shard_collection.sockets[node_id]
//          }
//          try {
//            socket!!.waitForState().withTimeout(5000L, "socket wait").await()
//            logger.debug(marker, "connected to shard ${message.shard_id} on node $node")
//          }
//          catch (ex: Exception) {
//            logger.debug(marker, "could not connect to node: $node -> ${socket?.service_state} -> ${socket?.socket_state}")
//            throw ex
//          }
        }
        val socket = shard_collection.sockets[node_id]
        if (socket == null) {
          logger.info(marker, "shard node not found for shard: ${message.shard_id}")
          continue
        }

        try {
          val result = socket.ask<Any?>(message, total_timeout = 5000, retry_timeout = 4000).await()
          logger.debug(marker, "successful response from shard ${message.shard_id}")
          return@future result
        }
        catch (ex: MessageTimeoutException) {
          logger.debug(marker, "shard message timed out from ${socket.identity} -> ${socket.socket_state}")
        }
        catch (ex: ShardNotFoundException) {
          if (!ex.ask_master) throw ex
          logger.info(marker, "shard not found. retrying..")
          shards[message.shard_id] = INACTIVE
        }
      }
    }.whenComplete { result, error ->
      val response = result ?: error
      when {
        response is Throwable -> {
          ask?.completeExceptionally(response)
          if (ask == null) {
            logger.warn(marker, "error with no recovery", response)
          }
        }
        ask != null           -> ask.complete(result)
      }
    }
    return true
  }

  private suspend fun connectToNodeAndEnsure(node: WorkerNode): Socket {
    val node_id = shardWorkerIdentifier(node.identifier)
    val check = try {
      shard_collection.sockets[node_id] ?: shard_collection.connect(node.host, node.port, node_id)
    }
    catch (ex: IllegalStateException) {
      shard_collection.sockets[node_id]!!
    }
//    try {
//      check.waitForState().withTimeout(3000).await()
//    }
//    catch (ex: Exception) {
//      logger.debug(marker, "could not connect to node: $node -> ${check.service_state} -> ${check.socket_state}")
//      throw ex
//    }
    return check
  }
}
