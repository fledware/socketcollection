package fledware.socketcollection.shard

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.SimpleServiceState
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.SocketState
import fledware.socketcollection.extend
import fledware.socketcollection.factory
import fledware.socketcollection.identities
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.internal.currentTime
import fledware.socketcollection.pubsub.PubSubClient
import fledware.socketcollection.withTimeout
import kotlinx.coroutines.experimental.CancellationException
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.channels.ClosedSendChannelException
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.atomic.AtomicLong


// ========================================================
// data types specific to the master
// ========================================================

private val NODES_RUNNING = setOf(SocketState.SHAKING, SocketState.NORMAL)

var MutableMap<String, Any>.node_data: WorkerNodeData?
  get() = this["data"] as WorkerNodeData?
  set(value) {
    if (value == null) throw IllegalArgumentException("cannot be null")
    this["data"] = value
  }

// TODO: we need to add a way to tell if a node is "stable".. basically, if a 
// TODO: single node returns and comes back over and over, we shouldn't bring
// TODO: the connection to an UNSTABLE state. or maybe DOWN_UNSTABLE
data class WorkerNodeData(val identifier: String,
                          val host: String,
                          val port: Int,
                          var capacity: Int,
                          var draining: Boolean = false,
                          val shards: ConcurrentSkipListSet<String> = ConcurrentSkipListSet())

fun WorkerNodeData.toWorkerNode() = WorkerNode(
    identifier = identifier,
    host = host,
    port = port
)


private typealias WorkerNodeComp = (WorkerNodeData, WorkerNodeData) -> Boolean

sealed class ShardJobDetails

object ShardJobDeleteDetails
  : ShardJobDetails()

data class ShardJobEnsureDetails(val node_id: String?)
  : ShardJobDetails()

data class ShardJobData(val job_id: UUID,
                        val job: Job,
                        val details: ShardJobDetails,
                        val promise: Promise<Unit>) {
  val is_finished: Boolean get() = promise.isDone || job.isCompleted
  var job_state: Any? = null
  suspend fun cancel() {
    promise.cancel(true)
    job.cancel()
    job.join()
  }
}

data class ShardData(val identifier: String,
                     val ghost: Boolean,
                     var deleting: Boolean,
                     var metric: Int,
                     var node_on: String?,
                     var job: ShardJobData? = null)


// ========================================================
// internal protocol specific to the master
// ========================================================

private sealed class InternalCommand {

  data class NodeStateChange(val node_id: String)

  data class NodeUnexpectedLeft(val node: WorkerNodeData)

  data class RequestShardDestroy(val shard_id: String,
                                 val promise: Promise<Unit>)

  data class RequestShardEnsure(val shard_id: String,
                                val promise: Promise<Unit>)

  data class InvalidShardState(val shard_id: String)
  
  data class ShardJobFinished(val shard_id: String,
                              val job_id: UUID)

  object DoFillBalance

}


// ========================================================
// actual implementation
// ========================================================

// TODO: we need to figure out a way for the master to leave
// TODO: and come back without thrashing.. so that the shards
// TODO: can stay in a semi stable state.
// - maybe we can add a node_seed: () -> Promise<Set<String>>
//   along with shard_seed. this will allow us to save the latest
//   state of a cluster and when it comes back up it starts the 
//   sockets with the given data and just assumes a SHAKING state.
//   this will also allow nodes to "live" on a node on startup.
class ShardMaster(val config: ShardConfiguration,
                  val pubsub: PubSubClient,
                  val shard_seed: () -> Promise<Set<String>>,
                  private val marker: Marker?,
                  builder: ChannelInitFactory = ChannelInitFactoryDefault()) : SimpleServiceImpl(marker) {
  companion object {
    private val logger = LoggerFactory.getLogger(ShardMaster::class.java)
    private val SMALLEST: WorkerNodeComp = { l1, l2 -> l1.shards.size < l2.shards.size }
    private val LARGEST: WorkerNodeComp = { l1, l2 -> l1.shards.size > l2.shards.size }
    private val RUNNING = setOf(SimpleServiceState.STARTING, SimpleServiceState.RUNNING)
  }

  val world_collection = factory.collectionAsServer(config.master_bind_world,
                                                    name = "${config.shard_name}-master-world",
                                                    marker = marker.extend("world"),
                                                    start_strategy = SimpleServiceStart.LAZY,
                                                    builder = builder,
                                                    socket_state = this::worldSocketStateChange,
                                                    handler = { s, m, i -> worldHandleInbound(s, m, i) })

  val shard_collection = factory.collectionAsServer(config.master_bind_shard,
                                                    name = "${config.shard_name}-master-shard",
                                                    marker = marker.extend("shard"),
                                                    start_strategy = SimpleServiceStart.LAZY,
                                                    config = SocketCollectionConfig(
                                                        socket_message_buffer = 1000,
                                                        socket_force_reconnect = false,
                                                        collection_message_buffer = -1,
                                                        collection_remove_on_down = false,
                                                        tell_acknowledged = true
                                                    ),
                                                    builder = builder,
                                                    socket_state = this::shardSocketStateChange,
                                                    handler = { s, m, i -> shardHandleInbound(s, m, i) })

  val shards = ConcurrentHashMap<String, ShardData>()
  val shard_validating = ConcurrentSkipListSet<String>()
  val publish_id = identities.generate()
  val publish_sort = AtomicLong()

  override fun actualStart(): Promise<Unit> {
    return future {
      logger.warn(marker, "${config.shard_name}-master starting")
      balancer.start()
      validator.start()
      shard_collection.start().await()
      world_collection.start().await()
      performSeedCheck()
      Unit
    }.whenComplete { _, th ->
      if (th != null) {
        logger.error(marker, "error while starting", th)
      }
      else {
        logger.warn(marker, "${config.shard_name}-master started")
      }
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    return future {
      logger.warn(marker, "${config.shard_name}-master shutting down")
      validator.cancel()
      validator.join()
      balancer.close()
      balancer.join()
      shards.values.forEach { it.job?.cancel() }
      shards.clear()
      world_collection.shutdown().await()
      shard_collection.shutdown().await()
      logger.warn(marker, "${config.shard_name}-master shut down")
      Unit
    }
  }

  /**
   *
   */
  // ======================================================
  // world endpoints
  // ======================================================
  /**
   *
   */

  private fun worldSocketStateChange(socket: Socket) {
    when (socket.socket_state) {
      SocketState.SHAKING      -> Unit
      SocketState.NORMAL       -> Unit
      SocketState.CLOSED       -> Unit
      SocketState.DOWN         -> Unit
      SocketState.DOWN_FINDING -> Unit
    }
  }

  @Suppress("UNUSED_PARAMETER")
  suspend fun worldHandleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    when (message) {
      is ShardProtocol.GetNodes     -> {
        return shard_collection.sockets.values
            .mapNotNull { it.data.node_data }
            .map { it.toWorkerNode() to it.shards }
      }
      is ShardProtocol.GetShardNode -> {
        val shard = shards[message.shard_id] ?: return ShardNotFoundException(message.shard_id, false)
        return shard.findShardNode()
      }
      is ShardProtocol.ShardEnsure  -> {
        val shard = shards[message.shard_id]
        if (shard != null) return shard.findShardNode()
        val request = InternalCommand.RequestShardEnsure(message.shard_id, Promise())
        balancer.send(request)
        request.promise.withTimeout(60_000L).await()
        val check = shards[message.shard_id] ?: return IllegalStateException("shard not created")
        return check.findShardNode()
      }
      is ShardProtocol.ShardDestroy -> {
        if (!shards.containsKey(message.shard_id)) return null
        val request = InternalCommand.RequestShardDestroy(message.shard_id, Promise())
        balancer.send(request)
        request.promise.withTimeout(60_000L).await()
        return null
      }
      else                          -> {
        logger.warn(marker, "unhandled message [1]: $message")
        return IllegalArgumentException("unhandled message")
      }
    }
  }

  private suspend fun ShardData.findShardNode(): WorkerNode {
    // we want to try a finite amount of times
    var attempt = 0
    while (attempt < 5) {
      val node: WorkerNodeData? = node_on?.let { shard_collection.sockets[it]?.data?.node_data }
      if (node != null) return node.toWorkerNode()
      
      val job = job
      if (job == null) {
        // if we get here, then just delay for a little bit and try again..
        // there could be a small amount of time when the balancer deletes
        // the move job and restarted it.. or the shard could be marked for
        // deletion.
        logger.warn("no job created for shard to find node")
        delay(500L)
        attempt++
        continue
      }
      if (deleting) throw ShardNotFoundException(this.identifier, false)
      when (job.details) {
        is ShardJobDeleteDetails -> throw ShardNotFoundException(this.identifier, false)
        is ShardJobEnsureDetails -> Unit
      }
      try {
        job.promise.withTimeout(2000).await()
      }
      catch (ex: Exception) {
        attempt++
        continue
      }
      
      // if the job completed and we still dont have a node, then we
      // know that the shard was not created or the job got cancelled.
      // so we just try again
      val node_on = node_on
      if (node_on == null) {
        attempt++
        continue
      }
      val check = shard_collection.sockets[node_on]?.data?.node_data ?:
                  throw ShardNotFoundException(this.identifier, true)
      return check.toWorkerNode()
    }
    logger.error(marker, "could not find node: $this")
    error("could not find node")
  }

  /**
   *
   */
  // ======================================================
  // shard endpoints
  // ======================================================
  /**
   *
   */

  private fun shardSocketStateChange(socket: Socket) {
    // lets make sure that the node is part of the cluster
    val node = shard_collection.sockets[socket.identity]?.data?.node_data
    when (socket.socket_state) {
      SocketState.SHAKING      -> {
        logger.warn(marker, "server connection is down")
      }
      SocketState.NORMAL       -> {
        logger.warn(marker, "server back up")
        if (node != null) {
          balancer.offer(InternalCommand.NodeStateChange(socket.identity))
        }
      }
      SocketState.DOWN         -> {
        logger.warn(marker, "node is down")
        if (node != null) {
          balancer.offer(InternalCommand.NodeStateChange(socket.identity))
        }
        else {
          // TODO: needs to test that this actually removed the node
          socket.shutdown()
        }
      }
      SocketState.DOWN_FINDING -> {

      }
      SocketState.CLOSED       -> {
        logger.warn(marker, "server left us for good")
        if (node != null) {
          balancer.offer(InternalCommand.NodeUnexpectedLeft(node))
        }
      }
    }
  }

  @Suppress("UNUSED_PARAMETER")
  suspend fun shardHandleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    try {
      when (message) {
        is ShardProtocol.WorkerJoining -> balancer.send(message)
        is ShardProtocol.WorkerDrain   -> balancer.send(message)
        is ShardProtocol.WorkerLeaving -> balancer.send(message)
        is ShardProtocol.WorkerMetrics -> balancer.send(message)
        else                           -> {
          logger.warn(marker, "unhandled message [1]: $message")
          return IllegalArgumentException("unhandled message")
        }
      }
      return null
    }
    catch (ex: ClosedSendChannelException) {
      return IllegalStateException("master shutdown")
    }
  }

  /**
   *
   */
  // ======================================================
  // balancer
  // ======================================================
  /**
   *
   */

  private val balancer = actor<Any>(CommonPool, 128, CoroutineStart.LAZY) {
    for (message in channel) {
      if (!isServiceState(RUNNING)) return@actor
      try {
        when (message) {
          is InternalCommand.NodeUnexpectedLeft  -> requestNodeUnexpectedLeft(message)
          is InternalCommand.DoFillBalance       -> performNodeBalancing()
          is InternalCommand.RequestShardDestroy -> requestShardDestroy(message)
          is InternalCommand.RequestShardEnsure  -> requestShardEnsure(message)
          is InternalCommand.InvalidShardState   -> requestShardInvalidState(message)
          is InternalCommand.NodeStateChange     -> requestNodeStateChange(message)
          is InternalCommand.ShardJobFinished    -> requestShardJobFinished(message)
          is ShardProtocol.WorkerJoining         -> requestWorkerJoining(message)
          is ShardProtocol.WorkerDrain           -> requestWorkerDraining(message)
          is ShardProtocol.WorkerLeaving         -> requestWorkerLeft(message)
          is ShardProtocol.WorkerMetrics         -> requestWorkerMetrics(message)
          else                                   -> {
            logger.warn(marker, "unknown message to balancer: $message")
          }
        }
      }
      catch (ex: Exception) {
        // TODO: signal to kill the system..?
        logger.error("error in balancer for message: $message", ex)
      }
    }
  }

  private suspend fun requestNodeUnexpectedLeft(message: InternalCommand.NodeUnexpectedLeft) {
    performNodeDrain(message.node)
    publishNodeLeft(message.node)
  }

  private suspend fun requestShardInvalidState(message: InternalCommand.InvalidShardState) {
    try {
      val shard = shards[message.shard_id]
      if (shard == null) {
        // if the shard doesnt exist, then we may need to tell 
        // nodes to remove the shard
        val removing = shard_collection.sockets.values.filter {
          val data = it.data.node_data ?: return@filter false
          return@filter data.shards.contains(message.shard_id)
        }.map { it.identity }
        if (removing.isEmpty()) return
        val ghost = ShardData(message.shard_id, true, true, 0, null)
        shards.put(message.shard_id, ghost)
        shardEnsureDeadState(ghost, "validator-found-ghost")
        return
      }

      val active_nodes = findActiveNodes()
      val registered_nodes = allRegisteredNodes()
      val shard_validator = all_validators.find { it.isInvalid(shard, active_nodes, registered_nodes) }
      if (shard_validator != null) {
        shard_validator.performFix(shard, active_nodes, registered_nodes)
        val shard_check = all_validators.find { it.isInvalid(shard, active_nodes, registered_nodes) }
        if (shard_check != null) {
          logger.info(marker, "shard is still invalid (${shard.identifier} -> ${shard_check.name}): shard -> $shard")
          return
        }
      }
      logger.info(marker, "was able to move shard to valid state: $shard")
    }
    finally {
      shard_validating -= message.shard_id
    }
  }

  private suspend fun requestWorkerJoining(message: ShardProtocol.WorkerJoining) {
    val socket = shard_collection.sockets[message.identifier]
    if (socket == null) {
      logger.warn(marker, "socket is null right after sending worker joining? $message")
      return
    }
    var node = socket.data.node_data ?: WorkerNodeData(message.identifier, message.host, message.port, message.capacity)
    node = node.copy(host = message.host, port = message.port, capacity = message.capacity, draining = false)
    socket.data.node_data = node
    logger.info(marker, "node joining: $node")
    publishNodeJoin(node)
    performNodeShardValidation(node, message.shards)
    performNodeBalancing()
  }

  private suspend fun requestWorkerMetrics(message: ShardProtocol.WorkerMetrics) {
    val socket = shard_collection.sockets[message.identifier] ?: return
    val node = socket.data.node_data
    if (node == null) {
      logger.warn(marker, "unable to find node on metrics send: $message")
      socket.shutdown()
      return
    }
    performNodeShardValidation(node, message.metrics.keys)
  }

  private suspend fun requestWorkerDraining(message: ShardProtocol.WorkerDrain) {
    val node = shard_collection.sockets[message.identifier]?.data?.node_data ?: return
    node.draining = true
    logger.info(marker, "node draining: $node")
    performNodeDrain(node)
  }

  private suspend fun requestWorkerLeft(message: ShardProtocol.WorkerLeaving) {
    val node = shard_collection.sockets[message.identifier]?.data?.node_data ?: return
    node.draining = true
    logger.info(marker, "node left: $node")
    performNodeDrain(node)
    publishNodeLeft(node)
  }

  private suspend fun requestShardEnsure(message: InternalCommand.RequestShardEnsure) {
    val check = shards[message.shard_id]
    if (check == null) {
      val shard = ShardData(message.shard_id, false, false, 0, null)
      shards.put(message.shard_id, shard)
      val node = findNode(is_better = SMALLEST)
      if (node != null) {
        shardEnsureLiveState(shard, node.identifier, "request-ensure")
      }
      else {
        logger.info(marker, "ensuring shard but no nodes available [${message.shard_id}]")
      }
      message.promise.complete(null)
    }
    else if (check.deleting) {
      logger.warn(marker, "ensure shard received on shard being deleted: $message")
      message.promise.completeExceptionally(IllegalStateException("shard being deleted"))
    }
    else {
      logger.info(marker, "ensure shard received on shard that already exists: $message")
      message.promise.complete(null)
    }
  }

  private suspend fun requestShardDestroy(message: InternalCommand.RequestShardDestroy) {
    val shard = shards[message.shard_id]
    if (shard != null) {
      shardEnsureDeadState(shard, "request-destroy")
    }
    message.promise.complete(null)
  }

  private suspend fun requestNodeStateChange(message: InternalCommand.NodeStateChange) {
    val socket = shard_collection.sockets[message.node_id] ?: return
    val node = socket.data.node_data ?: return
    when (socket.socket_state) {
      SocketState.SHAKING                  -> Unit
      SocketState.DOWN_FINDING             -> Unit
      SocketState.NORMAL                   -> {
        performNodeBalancing()
      }
      SocketState.DOWN, SocketState.CLOSED -> {
        performNodeDrain(node)
        publishNodeLeft(node)
      }
    }
  }

  private suspend fun performNodeShardValidation(node: WorkerNodeData, expected: Set<String>) {
    if (node.shards == expected) {
      logger.info(marker, "node shard list is valid")
      return
    }
    logger.warn(marker, "node shard validation failed... fixing")

    val leaving = expected - node.shards
    val adding = node.shards - expected

    for (shard_id in leaving) {
      val shard = shards[shard_id]
      if (shard == null) {
        logger.info(marker, "removing ghost shard from node: ${node.identifier} -> $shard_id")
        val ghost = ShardData(shard_id, true, true, 0, node.identifier)
        shards.put(shard_id, ghost)
        shardEnsureDeadState(ghost, "node-validation-ghost")
      }
      else if (shard.node_on == node.identifier) {
        // if the shard thinks its already on the given node, just let it stay
        node.shards += shard.identifier
      }
      else {
        logger.info(marker, "removing duplicate shard from node: ${node.identifier} -> $shard_id")
        shardEnsureLiveState(shard, shard.node_on, "node-validation-duplicate")
      }
    }

    for (shard_id in adding) {
      val shard = shards[shard_id]
      if (shard == null) {
        logger.warn(marker, "shard not found for adding to node, removing: ${node.identifier} -> $shard_id")
        node.shards -= shard_id
      }
      else {
        logger.info(marker, "adding expected shard to node: ${node.identifier} -> $shard_id")
        shardEnsureLiveState(shard, node.identifier, "node-validation-expected-not-found")
      }
    }
  }
  
  private suspend fun requestShardJobFinished(message: InternalCommand.ShardJobFinished) {
    val shard = shards[message.shard_id] ?: return
    val job = shard.job ?: return
    if (job.job_id == message.job_id) {
      shard.job = null
    }
  }

  private suspend fun performNodeDrain(node: WorkerNodeData) {
    node.draining = true
    val it = node.shards.iterator()
    while (it.hasNext()) {
      val shard = it.next().let { shards[it] }
      it.remove()

      // we have to move the shards off the node. so it doesnt really
      // matter if there isnt a node to go to, we need to empty the node...
      if (shard != null) {
        val moving_to = findNode(is_better = SMALLEST)
        shardEnsureLiveState(shard, moving_to?.identifier, "node-drain")
      }
    }
  }

  private suspend fun performNodeBalancing() {
    // first to perform the balance, we need to make sure that 
    // we have all the shards somewhere
    val valid_nodes = findActiveNodes().map { it.identifier }
    if (valid_nodes.isEmpty()) return
    val shard_it = shards.values.iterator()
    while (shard_it.hasNext() && is_running) {
      val shard = shard_it.next()
      if (shard.node_on != null || shard.job != null) continue
      if (shard.ghost || shard.deleting) continue
      val smallest = findNode(is_better = SMALLEST) ?: continue
      shardEnsureLiveState(shard, smallest.identifier, "balancing-0")
    }

    // now we want to check all the nodes that are down or left 
    // and ensure there are move jobs for them
    while (true && is_running) {
      val smallest = findNode(is_better = SMALLEST)
      val largest = findNode(is_better = LARGEST)
      if (smallest == null || largest == null) break
      if (largest.shards.isEmpty()) break
      if (smallest.identifier == largest.identifier) break
      val size_delta = largest.shards.size - smallest.shards.size
      if (size_delta > config.balancing_threshold) {
        // lets try to find shards that doesnt have a job
        var shard = largest.shards.find {
          val shard = shards[it] ?: return@find false
          return@find shard.job == null
        }?.let { shards[it] }
        if (shard != null) {
          shardEnsureLiveState(shard, smallest.identifier, "balancing-1")
          continue
        }

        // this means all the shards are currently being moved to the
        // given node... which is weird. so lets cancel the start job,
        // and put a remove job and a start job on the smallest node
        shard = largest.shards.first()?.let { shards[it] }
        if (shard == null) {
          logger.warn("shard null when largest is not empty?")
          continue
        }
        shardEnsureLiveState(shard, smallest.identifier, "balancing-2")
      }
      else {
        break
      }
    }
  }

  /**
   *
   */
  // ======================================================
  // validator
  // this constantly checks the state of shards and 
  // validates that if a shard is invalid, then there
  // is a job for changing the state to the correct node
  // ======================================================
  /**
   *
   */

  // TODO: we may want to make the validator run less if the machine this
  // TODO: is running on starts to get resource constrained... doing
  // TODO: these checks takes a lot of memory, and if there is a lot of
  // TODO: shards, then a lot of cpu as well.
  private val validator = launch(CommonPool, CoroutineStart.LAZY) {
    while (isServiceState(RUNNING)) {
      // we dont want to overload the balancer, so lets just go up
      // to the configured amount
      val invalid_shards = LinkedHashSet<String>()

      try {
        // first we want to make sure that there are no ghost shards
        val actual_shards = shard_collection.sockets.values
            .mapNotNull { it.data.node_data }
            .flatMapTo(LinkedHashSet()) { it.shards }
        val ghost_shards = actual_shards - shards.keys
        val ghost_it = ghost_shards.iterator()
        while (ghost_it.hasNext() && invalid_shards.size <= config.validation_max_size) {
          includeToValidateMaybe(ghost_it.next(), invalid_shards)
        }

        val active_nodes = findActiveNodes()
        val registered_nodes = allRegisteredNodes()
        val shard_it = shards.values.iterator()
        while (shard_it.hasNext() && invalid_shards.size <= config.validation_max_size) {
          val shard = shard_it.next()
          val invalid = all_validators.any { it.isInvalid(shard, active_nodes, registered_nodes) }
          if (invalid) {
            includeToValidateMaybe(shard.identifier, invalid_shards)
          }
        }
      }
      catch (ex: CancellationException) {
        return@launch
      }
      catch (ex: Exception) {
        logger.error(marker, "error while trying to validate shards", ex)
      }

      if (invalid_shards.isEmpty()) {
        logger.info(marker, "no invalid shards found")
        balancer.send(InternalCommand.DoFillBalance)
      }
      else {
        invalid_shards.forEach {
          // we just perform an offer because its not a big deal if all the 
          // messages get sent. they will be retried on the next iteration
          balancer.offer(InternalCommand.InvalidShardState(it))
        }
      }
      
      // now we just want to close all the nodes that havent joined but
      // are connected for some reason
      val current = currentTime()
      shard_collection.sockets.values.forEach {
        if (it.data.node_data == null &&
            it.info.created_at + config.master_unjoined_close_timeout < current) {
          logger.warn(marker, "closing channel because it hasnt joined yet: ${it.identity}")
          it.shutdown()
        }
      }

      delay(config.validation_delay)
    }
  }

  private fun includeToValidateMaybe(shard_id: String, buffer: LinkedHashSet<String>) {
    if (!shard_validating.contains(shard_id)) {
      shard_validating += shard_id
      buffer += shard_id
    }
  }

  private suspend fun performSeedCheck() {
    val set = shard_seed().await()
    for (shard_id in set) {
      val request = InternalCommand.RequestShardEnsure(shard_id, Promise())
      balancer.send(request)
      request.promise.withTimeout(60_000L).await()
    }
  }


  /**
   *
   */
  // ======================================================
  // shard validation and fixing
  // ======================================================
  /**
   *
   */

  private interface ShardValidator {

    val name: String
    suspend fun isInvalid(shard: ShardData,
                          active_nodes: List<WorkerNodeData>,
                          registered_nodes: List<WorkerNodeData>): Boolean

    suspend fun performFix(shard: ShardData,
                           active_nodes: List<WorkerNodeData>,
                           registered_nodes: List<WorkerNodeData>): Boolean
  }

  /**
   * the shard is not on a node and there are valid nodes, but there is no
   * job to start the shard
   */
  private val validator_orphaned = object : ShardValidator {
    override val name: String = "orphaned"

    override suspend fun isInvalid(shard: ShardData,
                                   active_nodes: List<WorkerNodeData>,
                                   registered_nodes: List<WorkerNodeData>): Boolean {
      return shard.node_on == null && shard.job == null && active_nodes.isNotEmpty()
    }

    override suspend fun performFix(shard: ShardData,
                                    active_nodes: List<WorkerNodeData>,
                                    registered_nodes: List<WorkerNodeData>): Boolean {
      val smallest = findNode(is_better = SMALLEST)
      if (smallest == null) {
        logger.warn(marker, "[$name] asked to perform fix but no active nodes")
        return false
      }
      logger.info(marker, "[$name] shard was orphaned. moving to valid node (${shard.identifier})")
      shardEnsureLiveState(shard, smallest.identifier, name)
      return true
    }
  }

  /**
   * the shard is a ghost (not a valid shard but is on nodes) and there is
   * no delete job
   */
  private val validator_ghost = object : ShardValidator {
    override val name: String = "ghost"

    override suspend fun isInvalid(shard: ShardData,
                                   active_nodes: List<WorkerNodeData>,
                                   registered_nodes: List<WorkerNodeData>): Boolean {
      return shard.ghost && shard.job?.details !is ShardJobDeleteDetails
    }

    override suspend fun performFix(shard: ShardData,
                                    active_nodes: List<WorkerNodeData>,
                                    registered_nodes: List<WorkerNodeData>): Boolean {
      logger.info(marker, "[$name] shard is a ghost with no delete job (${shard.identifier})")
      shardEnsureDeadState(shard, name)
      return true
    }
  }

  /**
   * happens when a shard is supposed to be in the process of deleting,
   * but there are no delete jobs for the shard
   */
  private val validator_deleting_no_job = object : ShardValidator {
    override val name: String = "deleting-no-job"

    override suspend fun isInvalid(shard: ShardData,
                                   active_nodes: List<WorkerNodeData>,
                                   registered_nodes: List<WorkerNodeData>): Boolean {
      return shard.deleting && shard.job?.details !is ShardJobDeleteDetails
    }

    override suspend fun performFix(shard: ShardData,
                                    active_nodes: List<WorkerNodeData>,
                                    registered_nodes: List<WorkerNodeData>): Boolean {
      shardEnsureDeadState(shard, name)
      logger.warn(marker, "[$name] shard was marked for deletion, but there was no delete job (${shard.identifier})")
      return true
    }
  }

  /**
   * if the shard.node_on is an invalid node
   */
  private val validator_on_invalid_node = object : ShardValidator {
    override val name: String = "on-invalid-node"

    override suspend fun isInvalid(shard: ShardData,
                                   active_nodes: List<WorkerNodeData>,
                                   registered_nodes: List<WorkerNodeData>): Boolean {
      val node_on = shard.node_on
      if (node_on == null) return false
      val job_details = shard.job?.details
      return when (job_details) {
        is ShardJobEnsureDetails -> {
          // we need to make sure that its going to a valid node
          val socket = shard_collection.sockets[job_details.node_id] ?: return true
          val node = socket.data.node_data ?: return true
          node.draining || !NODES_RUNNING.contains(socket.socket_state)
        }
        is ShardJobDeleteDetails -> {
          // just let it do its thing
          false
        }
        null                     -> {
          // make sure the current node is valid
          val socket = shard_collection.sockets[node_on] ?: return true
          val node = socket.data.node_data ?: return true
          node.draining || !NODES_RUNNING.contains(socket.socket_state)
        }
      }
    }

    override suspend fun performFix(shard: ShardData,
                                    active_nodes: List<WorkerNodeData>,
                                    registered_nodes: List<WorkerNodeData>): Boolean {
      val smallest = findNode(is_better = SMALLEST)
      shardEnsureLiveState(shard, smallest?.identifier, name)
      logger.warn(marker, "[$name] shard was found on an invalid node (${shard.identifier})")
      return true
    }
  }

  /**
   * the the shard.identity is in more than one node.shards and there is no job
   */
  private val validator_on_multiple_nodes = object : ShardValidator {
    override val name: String = "on-multiple-nodes"

    override suspend fun isInvalid(shard: ShardData,
                                   active_nodes: List<WorkerNodeData>,
                                   registered_nodes: List<WorkerNodeData>): Boolean {
      if (shard.job != null) return false
      val nodes_on = allRegisteredNodes().count { it.shards.contains(shard.identifier) }
      return nodes_on > 1
    }

    override suspend fun performFix(shard: ShardData,
                                    active_nodes: List<WorkerNodeData>,
                                    registered_nodes: List<WorkerNodeData>): Boolean {
      logger.warn(marker, "[$name] shard found on multiple nodes (${shard.identifier})")
      shardEnsureLiveState(shard, shard.node_on, name)
      return true
    }
  }

  private val all_validators = listOf(
      validator_orphaned,
      validator_ghost,
      validator_deleting_no_job,
      validator_on_invalid_node,
      validator_on_multiple_nodes
  )


  /**
   *
   */
  // ======================================================
  // coordinator jobs..
  // this handles actually doing the work along with
  // failure cases and retries
  // ======================================================
  /**
   *
   */

  private suspend fun shardEnsureLiveState(shard: ShardData, node_id: String?, reason: String) {
    val job = shard.job
    if ((job?.details as? ShardJobEnsureDetails)?.node_id == node_id && job?.is_finished == false) {
      logger.info(marker, "job for ensure already exists: $node_id")
      return
    }
    shard_collection.sockets.values.forEach {
      if (it.identity != node_id) it.data.node_data?.shards?.remove(shard.identifier)
    }
    if (node_id != null) {
      shard_collection.sockets[node_id]?.data?.node_data?.shards?.add(shard.identifier)
    }
    logger.info(marker, "ensuring shard ${shard.identifier}: $reason")
    shardActualJobStart(shard, ShardJobEnsureDetails(node_id), { s, j -> shardEnsureAttempt(s, j) })
  }

  private suspend fun shardEnsureDeadState(shard: ShardData, reason: String) {
    val job = shard.job
    if (job?.details is ShardJobDeleteDetails && job.is_finished == false) {
      logger.info(marker, "job for delete already exists: ${shard.identifier}")
      return
    }
    shard_collection.sockets.values.forEach {
      it.data.node_data?.shards?.remove(shard.identifier)
    }
    logger.info(marker, "deleting shard ${shard.identifier}: $reason")
    shardActualJobStart(shard, ShardJobDeleteDetails, { s, j -> shardDeleteAttempt(s, j) })
  }
  
  private suspend fun shardActualJobStart(shard: ShardData, details: ShardJobDetails, 
                                          block: suspend (shard: ShardData, job: ShardJobData) -> Boolean) {
    val job_id = UUID.randomUUID()
    val promise = Promise<Unit>()
    shard.job?.cancel()
    val actual_job = shardActualJob(shard.identifier, job_id, promise, block)
    shard.job = ShardJobData(job_id, actual_job, details, promise)
  }
  
  private fun shardActualJob(shard_id: String, job_id: UUID, promise: Promise<Unit>,
                             block: suspend (shard: ShardData, job: ShardJobData) -> Boolean) = launch(CommonPool) {
    // the only thing this job does is look through all the nodes and
    // make sure the shard doesnt exist anywhere
    while (isActive && is_running && !promise.isDone) {
      // first, we need to validate the job
      val shard = shards[shard_id] ?: throw CancellationException("unable to find shard")
      val job = shard.job ?: throw CancellationException("job was cancelled")
      if (job.job_id != job_id) throw CancellationException("job ids do not match")
      
      try {
        if (block(shard, job)) {
          logger.info(marker, "$shard_id job completed (${job.details})")
          promise.complete(null)
          balancer.send(InternalCommand.ShardJobFinished(shard_id, job_id))
          return@launch
        }
        delay(1000L)
      }
      catch (ex: CancellationException) {
        logger.info(marker, "$shard_id job was cancelled (${job.details}): ${ex.message}")
        promise.completeExceptionally(ex)
        return@launch
      }
      catch (ex: Throwable) {
        logger.warn(marker, "$shard_id job finished with exception (${job.details})", ex)
        promise.completeExceptionally(ex)
        return@launch
      }
    }
  }
  
  private suspend fun shardClearFromNodes(shard: ShardData, state: MutableSet<String>, nodes: MutableList<WorkerNodeData>) {
    // TODO: make a nodes.shards_actual so we dont have to hit every node every time 
    val node_it = nodes.iterator()
    while(node_it.hasNext()) {
      val node = node_it.next()
      if (coordinatorRemoveFromNode(shard, node.identifier)) {
        node_it.remove()
        state += node.identifier
      }
      delay(50)
    }
  }

  private suspend fun shardEnsureAttempt(shard: ShardData, job: ShardJobData): Boolean {
    // make sure once we successfully remove a shard from a node, that we
    // dont try to do it again
    @Suppress("UNCHECKED_CAST")
    val job_state = job.job_state as? LinkedHashSet<String> ?: LinkedHashSet()
    job.job_state = job_state
    val node_going_to = (job.details as ShardJobEnsureDetails).node_id

    // make sure the shard is not on any nodes its not supposed to be on
    val active_nodes = allRegisteredNodes()
        .filter { !job_state.contains(it.identifier) && node_going_to != it.identifier }
        .toMutableList()
    shardClearFromNodes(shard, job_state, active_nodes)
    if (active_nodes.isNotEmpty()) return false

    // now we need to make sure that the node it is currently on
    val node_on = shard.node_on
    if (node_on != null && node_on != node_going_to) {
      if (!coordinatorRemoveFromNode(shard, node_on)) {
        return false
      }
    }
    // now we need to make sure that the shard is actually on the node we want it to be on
    return node_going_to == null || coordinatorAddToNode(shard, node_going_to)
  }

  private suspend fun shardDeleteAttempt(shard: ShardData, job: ShardJobData): Boolean { 
    // make sure once we successfully remove a shard from a node, that we
    // dont try to do it again
    @Suppress("UNCHECKED_CAST")
    val job_state = job.job_state as? LinkedHashSet<String> ?: LinkedHashSet()
    job.job_state = job_state

    // now make sure the shard is not on any node
    val active_nodes = allRegisteredNodes()
        .filter { !job_state.contains(it.identifier) }
        .toMutableList()
    shardClearFromNodes(shard, job_state, active_nodes)
    if (active_nodes.isNotEmpty()) return false

    // finally delete the shard
    shards.remove(shard.identifier)
    publishShardDeleted(shard.identifier)
    return true
  }

  private suspend fun coordinatorRemoveFromNode(shard: ShardData, node_id: String): Boolean {
    logger.info(marker, "attempting to remove shard from node: ${shard.identifier} -> $node_id")
    val socket = shard_collection.sockets[node_id]
    val node = socket?.data?.node_data
    val finished: Boolean = when {
      socket == null -> {
        logger.info(marker, "couldnt find connected shard $node_id: ${shard.identifier}")
        true
      }
      node == null   -> {
        logger.info(marker, "couldnt find node ${shard.node_on}: ${shard.identifier}")
        true
      }
      else           -> {
        try {
          when (socket.socket_state) {
            SocketState.NORMAL       -> socket.ask<Boolean>(ShardProtocol.ShardStop(shard.identifier)).await()
            SocketState.SHAKING      -> false
            SocketState.DOWN         -> true
            SocketState.DOWN_FINDING -> true
            SocketState.CLOSED       -> true
          }
        }
        catch (ex: Exception) {
          logger.warn(marker, "error while trying to remove: $ex")
          false
        }
      }
    }
    if (finished) {
      logger.info(marker, "successfully removed shard from node $node_id: ${shard.identifier}")
      if (shard.node_on == node_id) shard.node_on = null
      node?.shards?.remove(shard.identifier) // just to make sure
      publishShardRemoved(shard.identifier, node_id)
      return true
    }
    else {
      logger.info(marker, "could not remove shard from node $node_id: ${shard.identifier}")
      return false
    }
  }

  private suspend fun coordinatorAddToNode(shard: ShardData, node_id: String): Boolean {
    logger.info(marker, "attempting to add shard to node: ${shard.identifier} -> $node_id")
    val socket = shard_collection.sockets[node_id]
    if (socket == null) {
      logger.warn(marker, "invalid state ${shard.identifier} -> $node_id.. node data exists for non existent socket")
      return false
    }
    val node = socket.data.node_data
    if (node == null) {
      logger.warn(marker, "invalid state ${shard.identifier} -> $node_id.. node doesnt exist for add")
      return false
    }
    if (node.draining) {
      logger.warn(marker, "invalid state ${shard.identifier} -> $node_id.. node is draining")
      return false
    }
    if (shard.node_on != null && shard.node_on != node_id) {
      logger.warn(marker, "invalid state ${shard.identifier} -> $node_id.. shard is currently on node ${shard.node_on}")
      return false
    }
    val finished: Boolean = try {
      when (socket.socket_state) {
        SocketState.NORMAL       -> socket.ask<Boolean>(ShardProtocol.ShardStart(shard.identifier)).await()
        SocketState.SHAKING      -> false
        SocketState.DOWN         -> false
        SocketState.DOWN_FINDING -> false // shouldnt ever get here
        SocketState.CLOSED       -> false
      }
    }
    catch (ex: Exception) {
      logger.warn(marker, "error while trying to add shard: ${ex.message}")
      false
    }
    if (finished) {
      logger.info(marker, "successfully added shard to node ${node.identifier}: ${shard.identifier}")
      shard.node_on = node.identifier
      node.shards += shard.identifier
      publishShardStarted(shard.identifier, node)
      return true
    }
    else {
      logger.info(marker, "could not add shard to node ${node.identifier}: ${shard.identifier}")
      return false
    }
  }


  /**
   *
   */
  // ======================================================
  // utilities
  // ======================================================
  /**
   *
   */

  private fun allRegisteredNodes(): List<WorkerNodeData> {
    return shard_collection.sockets.values.mapNotNull { it.data.node_data }
  }

  private fun findActiveNodes(): List<WorkerNodeData> {
    return shard_collection.sockets.values.mapNotNull {
      if (!NODES_RUNNING.contains(it.socket_state)) return@mapNotNull null
      val node = it.data.node_data ?: return@mapNotNull null
      if (node.draining) return@mapNotNull null
      return@mapNotNull node
    }
  }

  private fun findNode(states: Set<SocketState> = NODES_RUNNING,
                       drain_allowed: Boolean = false,
                       is_better: WorkerNodeComp): WorkerNodeData? {
    var current: WorkerNodeData? = null
    for (socket in shard_collection.sockets.values) {
      val node = socket.data.node_data ?: continue
      if (!drain_allowed && node.draining) continue
      if (!states.contains(socket.socket_state)) continue
      if (current == null || is_better(node, current)) current = node
    }
    return current
  }

  private fun publishShardDeleted(shard_id: String) {
    pubsub.publish(publishShardTopics(shard_id),
                   ShardEvents.ShardDeleted(config.shard_name,
                                            publish_id,
                                            publish_sort.getAndIncrement(),
                                            shard_id))
  }

  private fun publishShardRemoved(shard_id: String, node_id: String) {
    pubsub.publish(publishShardTopics(shard_id),
                   ShardEvents.ShardRemoved(config.shard_name,
                                            publish_id,
                                            publish_sort.getAndIncrement(),
                                            shard_id,
                                            node_id))
  }

  private fun publishShardStarted(shard_id: String, node: WorkerNodeData) {
    pubsub.publish(publishShardTopics(shard_id),
                   ShardEvents.ShardStarted(config.shard_name,
                                            publish_id,
                                            publish_sort.getAndIncrement(),
                                            shard_id,
                                            node.identifier,
                                            node.host,
                                            node.port))
  }

  private fun publishNodeLeft(node: WorkerNodeData) {
    pubsub.publish(publishNodeTopics(),
                   ShardEvents.NodeLeft(config.shard_name,
                                        publish_id,
                                        publish_sort.getAndIncrement(),
                                        node.identifier))
  }

  private fun publishNodeJoin(node: WorkerNodeData) {
    pubsub.publish(publishNodeTopics(),
                   ShardEvents.NodeJoin(config.shard_name,
                                        publish_id,
                                        publish_sort.getAndIncrement(),
                                        node.identifier,
                                        node.host,
                                        node.port))
  }

  private fun publishNodeTopics() = setOf(
      ShardEvents.TOPIC,
      ShardEvents.topicShard(config.shard_name))

  private fun publishShardTopics(shard_id: String) = setOf(
      ShardEvents.TOPIC,
      ShardEvents.topicShard(config.shard_name),
      ShardEvents.topicShard(config.shard_name, shard_id))
}
