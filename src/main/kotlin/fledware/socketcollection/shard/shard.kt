package fledware.socketcollection.shard

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.Socket
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.pubsub.PubSubClient
import org.slf4j.Marker
import java.io.Serializable


interface Shard {
  val shard_id: String
  val metric: Int
  suspend fun start()
  suspend fun receive(socket: Socket, message: Any, is_ask: Boolean): Any?
  suspend fun close()
}

class ShardNotFoundException(val shard_id: String, val ask_master: Boolean)
  : Exception("$shard_id ($ask_master)")

data class WorkerNode(val identifier: String,
                      val host: String,
                      val port: Int) : Serializable

data class ShardConfiguration(val shard_name: String,
                              val min_nodes: Int = 1,
                              val master_bind_host: String = "localhost",
                              val master_bind_shard: Int = 7017,
                              val master_bind_world: Int = 7018,
                              val master_pub_shard_debug: Boolean = false,
                              val master_unjoined_close_timeout: Long = 10000,
                              val node_down_millis: Long = 20_000,
                              val leaving_wait_millis: Long = 10_000,
                              val metric_delay_millis: Long = 10_000,
                              val validation_delay: Long = 5000,
                              val validation_max_size: Int = 50,
                              val balancing_threshold: Int = 2)

sealed class ShardProtocol : Serializable {

  // ================================================================
  // worker -> master
  // ================================================================

  // when a worker is joining/ rejoining
  data class WorkerJoining(val identifier: String,
                           val capacity: Int,
                           val host: String,
                           val port: Int,
                           val shards: Set<String>)
    : ShardProtocol()

  // this is used when a worker wants to remove all of 
  // the shards it owns. generally use this when the node
  // is about to leave.
  data class WorkerDrain(val identifier: String)
    : ShardProtocol()

  // when a worker is leaving and the shards should
  // be moved to other nodes... this is considered immediate
  data class WorkerLeaving(val identifier: String)
    : ShardProtocol()

  // sent from the worker node to let the master know how things
  // are going. basically, there needs to be some measurement
  // for each shard it owns, and this reports those measurements.
  // the master can use this to figure out balancing.
  data class WorkerMetrics(val identifier: String,
                           val metrics: Map<String, Int>)
    : ShardProtocol()


  // ================================================================
  // master -> worker
  // ================================================================

  // a query to get metrics from a node.
  // TODO: this needs to be called by the master. and if timeouts
  // TODO: happen, then the shard is considered down as well.
  object GetMetrics
    : ShardProtocol()

  // lets a worker node know to start a shard
  data class ShardStart(val shard_id: String)
    : ShardProtocol()

  // lets a worker node know to stop and persist a shard
  data class ShardStop(val shard_id: String)
    : ShardProtocol()


  // ================================================================
  // world -> master
  // ================================================================

  // a query to get the known nodes. this should only be queried
  // for on the master
  object GetNodes
    : ShardProtocol()//, RequestAsk<Map<WorkerNode, Set<String>>>

  // a query to get the node information for a given shard
  data class GetShardNode(val shard_id: String)
    : ShardProtocol()//, RequestAsk<WorkerNode>

  // 
  data class ShardEnsure(val shard_id: String)
    : ShardProtocol()

  data class ShardDestroy(val shard_id: String)
    : ShardProtocol()


  // ================================================================
  // world -> worker
  // ================================================================

  // this message can only be sent to the worker. this means that
  // the client needs to figure out and track where shards live.
  // this can happen with directly asking the master, or by listening
  // the events after getting the initial state of the cluster.
  data class ShardMessage(val shard_id: String,
                          val message: Any,
                          val is_ask: Boolean)
    : ShardProtocol()

}

sealed class ShardEvents : Serializable {
  companion object {
    val TOPIC = "shard"
    fun topicShard(shard_name: String) = "shard-$shard_name"
    fun topicShard(shard_name: String, shard_id: String) = "shard-$shard_name-$shard_id"
  }

  data class NodeLeft(val shard_name: String,
                      val publish_id: String,
                      val publish_sort: Long,
                      val node_identity: String)
    : ShardEvents()

  data class NodeJoin(val shard_name: String,
                      val publish_id: String,
                      val publish_sort: Long,
                      val node_identity: String,
                      val node_host: String,
                      val node_port: Int)
    : ShardEvents()

  data class ShardDeleted(val shard_name: String,
                          val publish_id: String,
                          val publish_sort: Long,
                          val shard_id: String)
    : ShardEvents()

  data class ShardRemoved(val shard_name: String,
                          val publish_id: String,
                          val publish_sort: Long,
                          val shard_id: String,
                          val node_identity: String)
    : ShardEvents()

  data class ShardStarted(val shard_name: String,
                          val publish_id: String,
                          val publish_sort: Long,
                          val shard_id: String,
                          val node_identity: String,
                          val node_host: String,
                          val node_port: Int)
    : ShardEvents()
}

object shard_factory {
  
  fun master(config: ShardConfiguration,
             pubsub: PubSubClient,
             shard_seed: () -> Promise<Set<String>>,
             marker: Marker? = null,
             builder: ChannelInitFactory = ChannelInitFactoryDefault(),
             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE): ShardMaster {
    val result = ShardMaster(config, pubsub, shard_seed, marker, builder)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
  
  fun node(config: ShardConfiguration,
           self_host: String,
           self_port: Int,
           shard_creater: (String) -> Shard,
           kill: Promise<Unit>,
           marker: Marker? = null,
           builder: ChannelInitFactory = ChannelInitFactoryDefault(),
           start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE): ShardNode {
    val result = ShardNode(config, self_host, self_port, shard_creater, kill, marker, builder)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
  
  fun client(config: ShardConfiguration,
             pubsub: PubSubClient,
             marker: Marker? = null,
             builder: ChannelInitFactory = ChannelInitFactoryDefault(),
             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE): ShardClient {
    val result = ShardClient(config, pubsub, marker, builder)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
}
