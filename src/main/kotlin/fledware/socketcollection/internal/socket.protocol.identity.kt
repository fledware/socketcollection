package fledware.socketcollection.internal

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.util.Attribute
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.io.Serializable
import java.util.concurrent.atomic.AtomicReference


sealed class IdentityProtocol : Serializable {
  data class MyIdentity(val identity: String)
    : IdentityProtocol()

  data class Acknowledged(val identity: String)
    : IdentityProtocol()
}

private enum class IdentityState {
  SHAKING,
  NORMAL
}

class InboundIdentityAdapter(private val client_identity: String?) : ChannelInboundHandlerAdapter() {
  companion object {
    private val logger = LoggerFactory.getLogger("fledware.socketcollection.internal.protocol.identity")
  }
  
  private val state = AtomicReference<IdentityState>(IdentityState.SHAKING)
  private lateinit var identity_attr: Attribute<String>
  private lateinit var marker: Attribute<Marker?>

  override fun channelRead(context: ChannelHandlerContext, message: Any) {
    when (state.get()) {
      IdentityState.NORMAL  -> {
        if (message is IdentityProtocol.MyIdentity || message is IdentityProtocol.Acknowledged) {
          logger.error(marker.get(), "invalid message for normal state of identity protocol: $message")
          context.close()
          return
        }
        super.channelRead(context, message)
      }
      IdentityState.SHAKING -> {
        if (client_identity != null) {
          // we initiated
          if (message !is IdentityProtocol.Acknowledged) {
            logger.error(marker.get(), "invalid message for identity protocol [0]: $message")
            context.close()
            return
          }
          if (message.identity != client_identity) {
            logger.error(marker.get(), "invalid identity returned: $message")
            context.close()
            return
          }
          identity_attr.set(message.identity)
        }
        else {
          // we did not initiate
          if (message !is IdentityProtocol.MyIdentity) {
            logger.error(marker.get(), "invalid message for identity protocol [1]: $message")
            context.close()
            return
          }
          identity_attr.set(message.identity)
          context.writeFlushAndErrorClose(IdentityProtocol.Acknowledged(message.identity), logger, marker.get())
        }
        context.fireUserEventTriggered(SocketEvent.IdentityReceived(identity_attr.get()))
        state.set(IdentityState.NORMAL)
        context.fireChannelActive()
        logger.debug(marker.get(), "identity figured out: $message")
      }
      else                                         -> {
        logger.error(marker.get(), "invalid null state")
        context.close()
      }
    }
  }

  override fun channelActive(context: ChannelHandlerContext) {
    logger.trace(marker.get(), "channel active")
    state.set(IdentityState.SHAKING)
    if (client_identity != null) {
      context.writeFlushAndErrorClose(IdentityProtocol.MyIdentity(client_identity), logger, marker.get())
    }
  }

  override fun channelInactive(context: ChannelHandlerContext) {
    logger.trace(marker.get(), "channel inactive")
    state.set(IdentityState.SHAKING)
    super.channelInactive(context)
  }

  override fun handlerAdded(context: ChannelHandlerContext) {
    identity_attr = context.channel().attr(SocketKeys.IDENTITY)
    marker = context.channel().attr(SocketKeys.MARKER)
    super.handlerAdded(context)
  }
}
