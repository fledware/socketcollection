package fledware.socketcollection.internal

import fledware.socketcollection.runIfNotComplete
import io.netty.channel.ChannelDuplexHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelPromise
import io.netty.util.Attribute
import org.slf4j.LoggerFactory
import org.slf4j.Marker

class SocketHandlerStartAdapter(private val is_client: Boolean,
                                private val marker: Marker?) : ChannelDuplexHandler() {
  companion object {
    private val logger = LoggerFactory.getLogger("fledware.socketcollection.internal.protocol.begin")
  }
  
  private lateinit var attr_last_send: Attribute<Long>
  private lateinit var attr_last_receive: Attribute<Long>
  private lateinit var close_initiated: Attribute<Boolean>
  private lateinit var close_valid: Attribute<Boolean>
  
  override fun channelRead(context: ChannelHandlerContext, message: Any) {
    attr_last_receive.set(currentTime())
    if (message is PingProtocol.GoodBye) {
      close_valid.set(true)
      if (is_client) {
        if (close_initiated.get()) {
          // this means we sent the close message and the server responded.
          // so we want to send GoodByeAck and then close the connection
          logger.debug(marker, "GoodBye response from server received")
          context.writeFlushAndErrorClose(PingProtocol.GoodByeAck, logger, marker)
          context.close()
        }
        else {
          // the server has initiated the close of this connection. we just
          // want to send GoodByeAck and then close the channel
          logger.debug(marker, "server requesting close with GoodBye")
          context.writeFlushAndErrorClose(PingProtocol.GoodByeAck, logger, marker)
          context.close()
        }
      }
      else {
        if (close_initiated.get()) {
          // this means that the client returned an invalid response
          logger.warn(marker, "invalid GoodBye response from client... closing anyways")
          context.channel().close()
        }
        else {
          // this means the client is requesting a close. we need to relay the 
          // message and wait for GoodByeAck
          logger.debug(marker, "GoodBye from client received")
          context.writeFlushAndErrorClose(PingProtocol.GoodBye, logger, marker)
          
          // but just to make sure that something doesnt happen to the connection and 
          // the server socket is left open, we want to timeout after this amount of time
          context.channel().closeFuture().toPromise().runIfNotComplete(2_000) {
            logger.warn(marker, "client sent GoodBye, but didnt complete handshake... closing")
            context.channel().close()
          }
        }
      }
    }
    else if (message is PingProtocol.GoodByeAck) {
      close_valid.set(true)
      if (is_client) {
        logger.warn(marker, "invalid GoodByeAck response from server received... closing anyways")
      }
      else {
        logger.debug(marker, "server close shake is complete. closing..")
      }
      context.channel().close()
    }
    else {
      context.fireChannelRead(message)
    }
  }

  override fun userEventTriggered(context: ChannelHandlerContext, event: Any) {
    if (event is SocketEvent.CloseInitiate) {
      logger.debug(marker, "close of the socket has been initiated")
      close_initiated.set(true)
      close_valid.set(true) // we do this because we know we want to close the connection
      context.writeFlushAndErrorClose(PingProtocol.GoodBye, logger, marker)
    }
    super.userEventTriggered(context, event)
  }

  override fun write(ctx: ChannelHandlerContext, msg: Any, promise: ChannelPromise) {
    attr_last_send.set(currentTime())
    super.write(ctx, msg, promise)
  }

  override fun channelInactive(ctx: ChannelHandlerContext?) {
    logger.trace(marker, "channel inactive")
    super.channelInactive(ctx)
  }

  override fun channelActive(ctx: ChannelHandlerContext?) {
    logger.trace(marker, "channel active")
    super.channelActive(ctx)
  }

  override fun handlerAdded(context: ChannelHandlerContext) {
    context.channel().attr(SocketKeys.CONNECT_AT).set(currentTime())
    attr_last_send = context.channel().attr(SocketKeys.LAST_SEND).also { it.set(currentTime()) }
    attr_last_receive = context.channel().attr(SocketKeys.LAST_RECEIVE).also { it.set(currentTime()) }
    close_initiated = context.channel().attr(SocketKeys.CLOSE_INITIATED).also { it.set(false) }
    close_valid = context.channel().attr(SocketKeys.CLOSE_VALID).also { it.set(false) }
    context.channel().attr(SocketKeys.IS_CLIENT).set(is_client)
    context.channel().attr(SocketKeys.MARKER).set(marker)
  }
}
