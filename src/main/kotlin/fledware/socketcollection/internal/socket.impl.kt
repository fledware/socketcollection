package fledware.socketcollection.internal

import fledware.socketcollection.InboundHandler
import fledware.socketcollection.MessageRejectException
import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceState
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.SocketInfo
import fledware.socketcollection.SocketState
import fledware.socketcollection.SocketType
import fledware.socketcollection.promiseUnitComplete
import fledware.socketcollection.runIfNotComplete
import io.netty.channel.ChannelFuture
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.LinkedList
import java.util.NoSuchElementException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.ConcurrentLinkedQueue


class SocketImpl(override val identity: String,
                 override val config: SocketCollectionConfig,
                 override val info: SocketInfo,
                 private val state_change: (Socket) -> Unit,
                 private val marker: Marker?,
                 initial_socket_handler: SocketHandler?,
                 private val handler: InboundHandler) : SimpleServiceImpl(marker), Socket {

  companion object {
    private val logger = LoggerFactory.getLogger(SocketImpl::class.java)
    val RUNNING = setOf(SimpleServiceState.STARTING, SimpleServiceState.RUNNING)
  }

  @ChannelHandler.Sharable
  private inner class InputHandler : ChannelInboundHandlerAdapter() {
    override fun channelRead(context: ChannelHandlerContext, message: Any) {
      if (socket_state != SocketState.NORMAL) {
        // if a read gets here and the state is not normal, then we can say
        // that the connection started in some weird way and there is a bug
        // within the protocol. just close ethe connection because apparently
        // we dont know what we're doing.
        logger.warn(marker, "state not normal with $message: $socket_state")
        context.channel().close()
      }
      else {
        // we are exiting the socket specific stuff, so leave the handler
        // like this so we cant reference the context
        launchIncomingMessage(message)
      }
    }

    override fun userEventTriggered(context: ChannelHandlerContext, event: Any) {
      when (event) {
        is SocketEvent.CloseInitiate -> {
          logger.debug(marker, "close successfully initiated on channel")
        }
        is SocketEvent.SocketStateChange -> {
          logger.debug(marker, "successful state change: $event")
        }
        is SocketEvent.IdentityReceived  -> {
          logger.debug(marker, "finished had shake on identity: $event")
        }
        else                             -> {
          logger.warn(marker, "unknown event: $event")
        }
      }
    }

    override fun channelActive(context: ChannelHandlerContext) {
      withState { socket_state = SocketState.NORMAL }
      super.channelActive(context)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun exceptionCaught(context: ChannelHandlerContext, cause: Throwable) {
      logger.warn(marker, "unhandled exception.. closing connection and reconnecting", cause)
      // just close and then go back to shaking..
      // make sure the state is changed first so we are removed from the
      // collection router before closing the channel
      withState {
        if (socket_state != SocketState.DOWN_FINDING) {
          socket_state = SocketState.SHAKING
        }
      }
      context.channel().close()
    }

    override fun handlerRemoved(context: ChannelHandlerContext) {
      // the socket can be null if it is forced closed
      val socket = socket_handler
      if (socket != null && !socket.can_reconnect) {
        detachSocketHandle()
      }
      // we first want to know if the close was valid
      val is_valid = context.channel().attr(SocketKeys.CLOSE_VALID).get()
      if (is_valid == null || !is_valid) {
        logger.warn(marker, "connection closed in an invalid way")
        withState {
          when (service_state) {
            SimpleServiceState.NEW      -> {
              logger.error(marker, "should not have got here.. this is a bug")
              shutdown()
            }
            SimpleServiceState.STARTING -> withState { socket_state = SocketState.SHAKING }
            SimpleServiceState.RUNNING  -> withState { socket_state = SocketState.SHAKING }
            SimpleServiceState.CLOSING  -> forceClose()
            SimpleServiceState.CLOSED   -> forceClose()
          }
        }
      }
      else {
        logger.debug(marker, "connection closed in a valid way")
        if (config.socket_force_reconnect &&
            info.type == SocketType.OUTBOUND &&
            isServiceState(RUNNING)) {
          logger.info(marker, "configured to automatically reconnect")
          withState { socket_state = SocketState.DOWN_FINDING }
        }
        else {
          detachSocketHandle()
          withState { socket_state = SocketState.CLOSED }
        }
      }
    }
  }

  init {
    if (initial_socket_handler != null) {
      attachSocketHandle(initial_socket_handler)
    }
  }

  override val data: MutableMap<String, Any> by lazy { ConcurrentHashMap<String, Any>() }
  private var timeout_check: Job? = null
  private val input_handler = InputHandler()
  private val state_waits by lazy { ConcurrentLinkedQueue<Pair<SocketState, Promise<Unit>>>() }
  private var socket_handler: SocketHandler? = null
  private val buffered_messages by lazy { ConcurrentLinkedDeque<TellableAlgorithmData<*>>() }

  @Volatile
  override var socket_state = SocketState.SHAKING
    private set

  //

  internal fun attachSocketHandle(socket: SocketHandler): Boolean {
    if (socket.identity != identity) {
      throw IllegalArgumentException("identity mismatch: this($identity) != that(${socket.identity})")
    }
    if (socket_state == SocketState.CLOSED) {
      throw IllegalStateException("invalid state to receive a socket handler: $service_state")
    }
    if (this.socket_handler == null) {
      this.socket_handler = socket
      socket.channel_maybe?.pipeline()?.addLast(input_handler)
      socket.connect_notifier = {
        try {
          socket.channel_maybe?.pipeline()?.remove(input_handler)
        }
        catch (ex: NoSuchElementException) {
          /* we do this just to make sure we're not adding duplicates */
        }
        socket.channel_maybe?.pipeline()?.addLast(input_handler)
      }
      return true
    }
    return false
  }

  internal fun detachSocketHandle() {
    socket_handler?.stopEnsuring()
    try {
      socket_handler?.channel_maybe?.pipeline()?.remove(input_handler)
    }
    catch (ex: NoSuchElementException) { /* if we cant find the handler, we dont want to stop the program */
    }
    socket_handler?.connect_notifier = null
    this.socket_handler = null
  }

  //

  internal fun tellSafeDontBuffer(message: Any): ChannelFuture? {
    val socket = socket_handler
    val channel = socket?.channel_maybe
    if (socket == null || channel == null || socket_state != SocketState.NORMAL) {
      return null
    }
    else {
      return socket.channel.writeAndFlush(message)
    }
  }

  override fun tell(message: Any, total_timeout: Long, acknowledged: Boolean, ack_retry_timeout: Long): Promise<Unit> {
    val type = if (acknowledged) TellableAlgorithmType.ACK else TellableAlgorithmType.FORGET
    val attempt = TellableAlgorithmData<Unit>(message, type, total_timeout, ack_retry_timeout)
    actualSendAttempt(attempt)
    return attempt.result
  }

  override fun tell(message: Any, acknowledged: Boolean): Promise<Unit> {
    return tell(message, config.tell_total_timeout, acknowledged, config.tell_ack_retry_timeout)
  }

  override fun <T> ask(message: Any, total_timeout: Long, retry_timeout: Long): Promise<T> {
    val attempt = TellableAlgorithmData<T>(message, TellableAlgorithmType.ASK, total_timeout, retry_timeout)
    actualSendAttempt(attempt)
    return attempt.result
  }

  override fun <T> ask(message: Any): Promise<T> {
    return ask(message, config.ask_total_timeout, config.ask_retry_timeout)
  }

  private fun actualSendAttempt(send: TellableAlgorithmData<*>) {
    if (send.result.isDone) return
    if (!isServiceState(SocketImpl.RUNNING)) {
      send.result.completeExceptionally(MessageRejectException("closed [0] ($service_state)", send.message))
      return
    }
    val socket = socket_handler
    val channel = socket?.channel_maybe
    if (socket == null || channel == null || socket_state != SocketState.NORMAL) {
      // we cant send the message, so either throw or buffer
      if (config.socket_message_buffer > 0 &&
          config.socket_message_buffer > buffered_messages.size) {
        buffered_messages += send
        send.result.whenComplete { _, _ -> buffered_messages -= send }
      }
      else {
        send.result.completeExceptionally(MessageRejectException("buffer is full", send.message))
      }
    }
    else {
      @Suppress("UNCHECKED_CAST")
      val actual_message = when (send.type) {
        TellableAlgorithmType.FORGET -> send.message
        TellableAlgorithmType.ACK    -> SocketSendTell(send.message, send.result as Promise<Unit>)
        TellableAlgorithmType.ASK    -> SocketSendAsk(send.message, send.result as Promise<Any>)
      }
      socket.channel.writeAndFlush(actual_message).addListener {
        if (it.isSuccess) {
          when (send.type) {
            TellableAlgorithmType.FORGET                         -> send.result.complete(null)
            TellableAlgorithmType.ACK, TellableAlgorithmType.ASK -> {
              send.retry_timeout_job = launch(CommonPool) {
                delay(send.retry_timeout)
                if (!send.result.isDone) {
                  logger.debug(marker, "attempting to resend message")
                  actualSendAttempt(send)
                }
              }
            }
          }
        }
        else {
          // unless there was an exception... then just retry
          logger.warn("exception during channel write: ${it.cause()}")
          actualSendAttempt(send)
        }
      }
    }
  }

  //

  override fun actualStart(): Promise<Unit> {
    socket_handler?.startEnsuring()
    return promiseUnitComplete()
  }

  override fun actualShutdown(): Promise<Unit> {
    if (withState { socket_state = SocketState.CLOSED }) {
      logger.info(marker, "closing socket: $identity")

      socket_handler?.stopEnsuring()
      val channel = socket_handler?.channel_maybe
      if (channel != null) {
        channel.pipeline().fireUserEventTriggered(SocketEvent.CloseInitiate)
        val result = channel.closeFuture().toUnitPromise()
        result.runIfNotComplete(config.socket_close_timeout) {
          channel.close()
          withState { socket_state = SocketState.CLOSED }
        }
        return result
      }
      else {
        detachSocketHandle()
        withState { socket_state = SocketState.CLOSED }
        return promiseUnitComplete()
      }
    }
    else {
      logger.info(marker, "socket already closed or not started")
      return promiseUnitComplete()
    }
  }
  
  private fun forceClose() {
    socket_handler?.stopEnsuring()
    socket_handler?.channel_maybe?.let {
      it.close()
      withState { socket_state = SocketState.CLOSED }
    }
    detachSocketHandle()
  }

  //

  private fun launchIncomingMessage(message: Any) = launch(CommonPool) {
    when (message) {
      is SocketReceiveAsk -> {
        try {
          val result = handler(this@SocketImpl, message.ask, true)
          message.promise.complete(result)
        }
        catch (ex: Exception) {
          logger.error(marker, "unexpected error while handling message: ${message.ask}", ex)
          message.promise.completeExceptionally(ex)
        }
      }
      else                -> {
        try {
          handler(this@SocketImpl, message, false)
        }
        catch (ex: Exception) {
          logger.error(marker, "unexpected error while handling message: $message", ex)
        }
      }
    }
  }

  //

  override fun waitForState(state: SocketState): Promise<Unit> {
    val result = Promise<Unit>()
    withState {
      when (socket_state) {
        state              -> result.complete(null)
        SocketState.CLOSED -> result.completeExceptionally(IllegalStateException("closed"))
        else               -> {
          val wait = state to result
          state_waits += wait
          result.whenComplete { _, _ -> state_waits -= wait }
        }
      }
    }
    return result
  }

  @Synchronized
  private fun withState(block: () -> Unit): Boolean {
    val before_state = socket_state
    block()
    if (before_state != socket_state) {
      if (before_state == SocketState.CLOSED) {
        socket_state = SocketState.CLOSED
        logger.warn(marker, "not allowed to go out of closed state")
      }
      else {
        logger.debug(marker, "state transitioned; $socket_state")
        stateChangeCheck()
        return true
      }
    }
    return false
  }

  private fun stateChangeCheck() {
    socket_handler?.channel_maybe?.also {
      it.pipeline().fireUserEventTriggered(SocketEvent.SocketStateChange(socket_state))
    }
    state_change(this)
    cancelDisconnectState()
    when (socket_state) {
      SocketState.SHAKING      -> {
        goToDisconnectedState()
      }
      SocketState.NORMAL       -> {
        drainMessageBuffer()
      }
      SocketState.CLOSED       -> Unit
      SocketState.DOWN_FINDING -> Unit
      SocketState.DOWN         -> Unit
    }
    drainWaitsForState()
  }

  private fun drainWaitsForState() {
    val it = state_waits.iterator()
    while (it.hasNext()) {
      val check = it.next()
      if (socket_state == check.first) {
        it.remove()
        check.second.complete(null)
      }
      else if (socket_state == SocketState.CLOSED) {
        it.remove()
        check.second.completeExceptionally(IllegalStateException("closed"))
      }
    }
  }

  private fun drainMessageBuffer() {
    if (config.socket_message_buffer > 0 &&
        config.socket_message_buffer > buffered_messages.size) {
      val socket = socket_handler
      if (socket == null || socket_state != SocketState.NORMAL) {
        logger.error(marker, "invalid state to drain buffer")
        throw IllegalStateException("invalid state to drain buffer")
      }
      val messages = LinkedList<TellableAlgorithmData<*>>()
      while (buffered_messages.isNotEmpty()) {
        messages += buffered_messages.poll()
      }
      messages.forEach { actualSendAttempt(it) }
    }
  }

  private fun cancelDisconnectState() {
    timeout_check?.cancel()
    timeout_check = null
  }

  private fun goToDisconnectedState() {
    if (timeout_check != null) return
    timeout_check = launch(CommonPool) {
      delay(config.socket_down_after_millis)
      withState {
        if (socket_state == SocketState.SHAKING) {
          socket_state = SocketState.DOWN
        }
      }
      timeout_check = null
    }
  }

}
