package fledware.socketcollection.internal

import fledware.socketcollection.MessageTimeoutException
import fledware.socketcollection.Promise
import fledware.socketcollection.SocketState
import io.netty.channel.ChannelInboundHandler
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.serialization.ClassResolvers
import io.netty.handler.codec.serialization.ObjectDecoder
import io.netty.handler.codec.serialization.ObjectEncoder
import io.netty.handler.flush.FlushConsolidationHandler
import io.netty.util.AttributeKey
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.Marker

object SocketKeys {
  val MARKER: AttributeKey<Marker?> = AttributeKey.newInstance("marker")
  val CONNECT_AT: AttributeKey<Long> = AttributeKey.newInstance("connect-at")
  val LAST_SEND: AttributeKey<Long> = AttributeKey.newInstance("last-send")
  val LAST_RECEIVE: AttributeKey<Long> = AttributeKey.newInstance("last-receive")
  val IDENTITY: AttributeKey<String> = AttributeKey.newInstance("identity")
  val CLOSE_INITIATED: AttributeKey<Boolean> = AttributeKey.newInstance("close-initiated")
  val CLOSE_VALID: AttributeKey<Boolean> = AttributeKey.newInstance("close-valid")
  val IS_CLIENT: AttributeKey<Boolean> = AttributeKey.newInstance("is-client")
}

sealed class SocketEvent {
  data class SocketStateChange(val state: SocketState)
    : SocketEvent()

  data class IdentityReceived(val identity: String)
    : SocketEvent()
  
  object CloseInitiate
    : SocketEvent()
}

enum class SocketHandlerState {
  ENSURING,
  INACTIVE
}

interface SocketHandler {

  val identity: String
  val channel_maybe: SocketChannel?
  val channel: SocketChannel
    get() = channel_maybe ?: throw IllegalStateException("no current channel")
  val state: SocketHandlerState
  val can_reconnect: Boolean
  var connect_notifier: (() -> Unit)?

  fun startEnsuring()
  fun stopEnsuring()
}

// ========================================================
// algorithm for tells and asks
// ========================================================

enum class TellableAlgorithmType {
  FORGET,
  ACK,
  ASK
}

data class TellableAlgorithmData<T>(val message: Any,
                                    val type: TellableAlgorithmType,
                                    val total_timeout: Long,
                                    val retry_timeout: Long) {
  val result: Promise<T> = Promise()
  var retry_timeout_job: Job? = null

  val total_timeout_job: Job = launch(CommonPool) {
    delay(total_timeout)
    if (!result.isDone) result.completeExceptionally(MessageTimeoutException("total timeout: $message", message))
  }

  init {
    result.whenComplete { _, _ ->
      total_timeout_job.cancel()
      retry_timeout_job?.cancel()
      retry_timeout_job = null
    }
  }
}


// ========================================================
// channel init builders:
// there are a standard set of adapters for the channels,
// but to make it extensible we need to make it a builder
// ========================================================

interface ChannelInitFactory {
  /**
   * this is used to make the client (or outbound) low level netty protocol
   */
  fun buildClient(marker: Marker?, identity: String): ChannelInitializer<SocketChannel>

  /**
   * this is used to make the server (or inbound) low level netty protocol
   */
  fun buildServer(marker: Marker?, receiver: ChannelInboundHandler? = null): ChannelInitializer<SocketChannel>
}

data class ChannelInitConfig(val socket_check_delay: Long = 500L,
                             val socket_timeout: Long = 10_000L,
                             val socket_reconnect_timeout: Long = 10_000L,
                             val ping_quiet_period: Long = 5000L)

class ChannelInitFactoryDefault(val config: ChannelInitConfig = ChannelInitConfig()) : ChannelInitFactory {
  override fun buildClient(marker: Marker?, identity: String) = build(marker, identity, null)

  override fun buildServer(marker: Marker?, receiver: ChannelInboundHandler?) = build(marker, null, receiver)

  private fun build(marker: Marker?, identity: String?, receiver: ChannelInboundHandler?): ChannelInitializer<SocketChannel> {
    return object : ChannelInitializer<SocketChannel>() {
      override fun initChannel(ch: SocketChannel) {
        ch.pipeline().addLast(FlushConsolidationHandler())
        ch.pipeline().addLast(ObjectEncoder())
        ch.pipeline().addLast(ObjectDecoder(ClassResolvers.cacheDisabled(null)))
        ch.pipeline().addLast(SocketHandlerStartAdapter(identity != null, marker))
        ch.pipeline().addLast(InboundPingAdapter(config))
        ch.pipeline().addLast(DuplexAckAdapter())
        ch.pipeline().addLast(InboundIdentityAdapter(identity))
        if (receiver != null) ch.pipeline().addLast(receiver)
      }
    }
  }
}
