package fledware.socketcollection.internal

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import io.netty.bootstrap.Bootstrap
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.Channel
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.channel.ChannelOption
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.net.ConnectException
import java.util.UUID

// ========================================================
// client rebooter:
// this handles the crashing of a client socket
// and attempts to stay connected until told otherwise
// ========================================================

class ClientSocketHandler(private val host: String,
                          private val port: Int,
                          private val marker: Marker?,
                          override val identity: String,
                          private val builder: ChannelInitFactory) : SocketHandler {
  companion object {
    private val logger = LoggerFactory.getLogger(ClientSocketHandler::class.java)
  }

  override val can_reconnect: Boolean = true
  override var connect_notifier: (() -> Unit)? = null

  @Volatile
  override var channel_maybe: SocketChannel? = null
    private set

  @Volatile
  override var state: SocketHandlerState = SocketHandlerState.ENSURING
    private set

  override fun startEnsuring() {
    attemptConnect()
  }

  override fun stopEnsuring() {
    state = SocketHandlerState.INACTIVE
  }

  private fun attemptConnect() {
    future {
      var errors = 0
      while (true) {
        if (state != SocketHandlerState.ENSURING) {
          logger.debug(marker, "connection is no longer ensuring [0]")
          break
        }
        
        val bootstrap = Bootstrap()
        bootstrap.group(groups.worker)
        bootstrap.channel(NioSocketChannel::class.java)
        bootstrap.handler(builder.buildClient(marker, identity))
        bootstrap.option(ChannelOption.TCP_NODELAY, true)
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true)

        logger.debug(marker, "about to attempt connect")
        val start = currentTime()
        val future = bootstrap.connect(host, port)
        try {
          future.toPromise().await()
          logger.debug(marker, "connect successful in ${currentTime() - start} ms")
          finishConnect(future.channel() as SocketChannel)
          break
        }
        catch (ex: Throwable) {
          when (ex) {
            is ConnectException -> logger.warn(marker, "connection failed: {}", ex.message)
            else                -> logger.warn(marker, "connection failed for unknown reason", ex)
          }
          errors++
          if (state != SocketHandlerState.ENSURING) {
            logger.debug(marker, "connection is no longer ensuring [1]")
            break
          }
          else {
            val delaying = Math.min(20_000L, errors * 1000L - 500)
            logger.info(marker, "delaying before reconnect attempt [errors: $errors]: $delaying")
            delay(delaying) 
          }
        }
      }
    }.whenComplete { _, th ->
      if (th != null) {
        logger.error(marker, "raw error while attempting connect... aborting", th)
      }
    }
  }

  private fun finishConnect(channel: SocketChannel) {
    if (state == SocketHandlerState.ENSURING) {
      channel_maybe = channel
      connect_notifier?.invoke()
      channel.closeFuture().toPromise().whenComplete { _, th ->
        channel_maybe = null
        if (th != null) {
          logger.warn(marker, "error listening to close future", th)
        }
        if (state == SocketHandlerState.ENSURING) {
          logger.info(marker, "attempting reconnect")
          attemptConnect()
        }
        else {
          logger.info(marker, "not trying to reconnect")
        }
      }
    }
    else {
      channel_maybe?.shutdown()
      channel_maybe = null
    }
  }
}


// ========================================================
// server socket receiver:
// this binds to a port and hands off the
// ServerSocketHandler to the handler
// ========================================================

class ServerSocketReceiver(private val port: Int,
                           private val marker: Marker?,
                           private val builder: ChannelInitFactory,
                           private val receiver: (ServerSocketHandler) -> Unit) : SimpleServiceImpl(marker) {
  companion object {
    private val logger = LoggerFactory.getLogger(ServerSocketReceiver::class.java)
  }

  @ChannelHandler.Sharable
  private inner class SocketReceiver : ChannelInboundHandlerAdapter() {
    override fun channelActive(context: ChannelHandlerContext) {
      val identity = context.channel().attr(SocketKeys.IDENTITY).get() ?: UUID.randomUUID().toString()
      val handler = ServerSocketHandler(identity, context.channel() as SocketChannel, marker)
      receiver(handler)
      context.fireChannelActive()
      context.pipeline().remove(this)
    }
  }

  private val receiver_listener = SocketReceiver()

  @Volatile
  private var _channel: Channel? = null
  val channel: Channel
    get() = _channel ?: throw IllegalStateException("no state")

  override fun actualStart(): Promise<Unit> {
    return future {
      val bootstrap = ServerBootstrap()
      bootstrap.group(groups.boss, groups.worker)
      bootstrap.channel(NioServerSocketChannel::class.java)
      bootstrap.childHandler(builder.buildServer(marker, receiver_listener))
      bootstrap.childOption(ChannelOption.TCP_NODELAY, true)
      bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true)

      // Bind and start to accept incoming connections.
      logger.info(marker, "about to bind to $port")
      val start = currentTime()
      val future = bootstrap.bind(port)
      future.toPromise().await()
      logger.info(marker, "bind to $port successful in ${currentTime() - start} ms")
      _channel = future.channel()
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    return future {
      val channel = _channel ?: return@future
      _channel = null

      logger.info(marker, "about to unbind to $port")
      val start = currentTime()
      channel.close().toPromise().await()
      logger.info(marker, "unbind from $port took ${currentTime() - start} ms")
    }
  }
}


// ========================================================
// server socket handler:
// the server handler.. note, this handler can
// be removed and reattached multiple times with 
// new instances.
// ========================================================

class ServerSocketHandler(override val identity: String,
                          override var channel_maybe: SocketChannel?,
                          private val marker: Marker?) : SocketHandler {

  companion object {
    private val logger = LoggerFactory.getLogger(ServerSocketHandler::class.java)
  }

  override val can_reconnect: Boolean = false
  @Volatile
  override var state: SocketHandlerState = SocketHandlerState.ENSURING
    private set
  override var connect_notifier: (() -> Unit)? = null

  init {
    channel.closeFuture().toPromise().whenComplete { _, th ->
      channel_maybe = null
      if (th != null) {
        logger.warn(marker, "error listening to close future", th)
      }
    }
  }

  override fun startEnsuring() {

  }

  override fun stopEnsuring() {
    state = SocketHandlerState.INACTIVE
  }
}
