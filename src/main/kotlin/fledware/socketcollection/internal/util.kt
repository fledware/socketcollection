package fledware.socketcollection.internal

import fledware.socketcollection.Promise
import io.netty.channel.ChannelOutboundInvoker
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.util.concurrent.Future
import io.netty.util.internal.logging.InternalLoggerFactory
import org.slf4j.Logger
import org.slf4j.Marker
import java.io.Serializable
import java.nio.channels.ClosedChannelException
import java.util.concurrent.atomic.AtomicInteger

object Null : Serializable

internal fun currentTime() = System.currentTimeMillis()

internal fun randomInt(max: Int): Int = (Math.random() * max).toInt()

internal object groups {

  // being that this is the most we can generally get out of a system,
  // even if there are multiple instances running in the same process 
  // (like testing), we can just share the pools statically. these should
  // ONLY be used internally.

  val boss = NioEventLoopGroup(2)
  val worker = NioEventLoopGroup()

  init {
    InternalLoggerFactory.getDefaultFactory()
    Runtime.getRuntime().addShutdownHook(object: Thread() {
      override fun run() = shutdown()
    })
  }

  @Suppress("DEPRECATION")
  fun shutdown() {
    boss.shutdownNow()
    worker.shutdownNow()
  }
}

internal fun Future<*>.toUnitPromise(): Promise<Unit> {
  val result = Promise<Unit>()
  this.addListener {
    if (it.isSuccess) {
      result.complete(null)
    }
    else {
      result.completeExceptionally(it.cause())
    }
  }
  return result
}

internal fun <T> Future<T>.toPromise(): Promise<T> {
  val result = Promise<T>()
  this.addListener {
    try {
      if (it.isSuccess) {
        @Suppress("UNCHECKED_CAST")
        result.complete(it.now as T)
      }
      else {
        result.completeExceptionally(it.cause())
      }
    }
    catch (ex: Exception) {
      result.completeExceptionally(ex)
    }
  }
  return result
}

internal fun ChannelOutboundInvoker.writeFlushAndErrorClose(message: Any, logger: Logger, marker: Marker? = null) {
  writeAndFlush(message).addListener {
    if (!it.isSuccess && !it.isCancelled) {
      val cause = it.cause()
      if (cause is ClosedChannelException) {
        logger.warn(marker, "write failed because channel is closed: $message")
      }
      else {
        logger.warn(marker, "write failed: $message", cause)
      }
      close()
    }
  }
}

internal class RoutedObject<T> {

  @Volatile
  var routing: List<T> = listOf()
    private set
  private val index = AtomicInteger(0)

  val size get() = routing.size
  fun contains(obj: T): Boolean = routing.contains(obj)

  fun pick(): T? {
    while (true) {
      val routing = routing
      if (routing.isEmpty()) return null
      val result = Math.abs(index.getAndIncrement()) % routing.size
      return routing[result]
    }
  }

  @Synchronized
  fun remove(obj: T): Boolean {
    if (!routing.contains(obj)) {
      return false
    }
    routing -= obj
    return true
  }

  @Synchronized
  fun add(obj: T): Boolean {
    if (routing.contains(obj)) {
      return false
    }
    routing += obj
    return true
  }

  operator fun plusAssign(obj: T) {
    add(obj)
  }

  operator fun minusAssign(obj: T) {
    remove(obj)
  }
}
