package fledware.socketcollection.internal

import fledware.socketcollection.InboundHandler
import fledware.socketcollection.MessageRejectException
import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceState
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollection
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.SocketInfo
import fledware.socketcollection.SocketState
import fledware.socketcollection.SocketType
import io.netty.util.concurrent.Future
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.LinkedList
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque


class SocketCollectionImpl(override val name: String,
                           override val config: SocketCollectionConfig,
                           private val marker: Marker?,
                           private val builder: ChannelInitFactory,
                           private val socket_state: (Socket) -> Unit,
                           private val inbound_handler: InboundHandler)
  : SimpleServiceImpl(marker),
    SocketCollection {

  companion object {
    private val logger = LoggerFactory.getLogger(SocketCollectionImpl::class.java)
    private val ACCEPTING = setOf(SimpleServiceState.NEW,
                                  SimpleServiceState.STARTING,
                                  SimpleServiceState.RUNNING)
    private val CONNECT = setOf(SimpleServiceState.STARTING,
                                SimpleServiceState.RUNNING)
  }

  private val bindings by lazy { ConcurrentHashMap<Int, ServerSocketReceiver>() }
  override val bound_ports: Set<Int> get() = bindings.keys
  private val buffered_messages by lazy { ConcurrentLinkedDeque<TellableAlgorithmData<*>>() }
  private val routed = RoutedObject<SocketImpl>()
  override val sockets = ConcurrentHashMap<String, SocketImpl>()
  override val active get() = routed.routing

  //


  override fun tell(message: Any, total_timeout: Long, acknowledged: Boolean, ack_retry_timeout: Long): Promise<Unit> {
    val type = if (acknowledged) TellableAlgorithmType.ACK else TellableAlgorithmType.FORGET
    val attempt = TellableAlgorithmData<Unit>(message, type, total_timeout, ack_retry_timeout)
    actualSendAttempt(attempt)
    return attempt.result
  }

  override fun tell(message: Any, acknowledged: Boolean): Promise<Unit> {
    return tell(message, config.tell_total_timeout, acknowledged, config.tell_ack_retry_timeout)
  }

  override fun <T> ask(message: Any, total_timeout: Long, retry_timeout: Long): Promise<T> {
    val attempt = TellableAlgorithmData<T>(message, TellableAlgorithmType.ASK, total_timeout, retry_timeout)
    actualSendAttempt(attempt)
    return attempt.result
  }

  override fun <T> ask(message: Any): Promise<T> {
    return ask(message, config.ask_total_timeout, config.ask_retry_timeout)
  }

  private fun actualSendAttempt(send: TellableAlgorithmData<*>) {
    if (send.result.isDone) return
    if (!isServiceState(SocketImpl.RUNNING)) {
      send.result.completeExceptionally(MessageRejectException("closed [0]", send.message))
      return
    }
    val socket = routed.pick()
    if (socket == null) {
      // there are no valid sockets
      bufferSend(send)
      return
    }

    @Suppress("UNCHECKED_CAST")
    val actual_message = when (send.type) {
      TellableAlgorithmType.FORGET -> send.message
      TellableAlgorithmType.ACK    -> SocketSendTell(send.message, send.result as Promise<Unit>)
      TellableAlgorithmType.ASK    -> SocketSendAsk(send.message, send.result as Promise<Any>)
    }
    val check = socket.tellSafeDontBuffer(actual_message)
    if (check != null) {
      check.addListener { channelSendListener(it, send) }
      return
    }

    val available = ArrayList<SocketImpl>(active)
    while (available.isNotEmpty()) {
      val index = randomInt(available.size)
      val socket_maybe = available.removeAt(index)
      val result_check = socket_maybe.tellSafeDontBuffer(send.message)
      if (result_check != null) {
        result_check.addListener { channelSendListener(it, send) }
        return
      }
    }
    
    // all sockets are full and in a bad state..
    return bufferSend(send)
  }
  
  private fun channelSendListener(future: Future<*>, send: TellableAlgorithmData<*>) {
    if (future.isSuccess) {
      when (send.type) {
        TellableAlgorithmType.FORGET                         -> send.result.complete(null)
        TellableAlgorithmType.ACK, TellableAlgorithmType.ASK -> {
          send.retry_timeout_job = launch(CommonPool) {
            delay(send.retry_timeout)
            if (!send.result.isDone) actualSendAttempt(send)
          }
        }
      }
    }
    else {
      // unless there was an exception... then just retry
      logger.warn("exception during channel write: ${future.cause()}")
      actualSendAttempt(send)
    }
  }
  
  private fun bufferSend(send: TellableAlgorithmData<*>) {
    if (config.collection_message_buffer > 0 &&
        config.collection_message_buffer > buffered_messages.size) {
      buffered_messages += send
      send.result.whenComplete { _, _ -> buffered_messages -= send }
    }
    else {
      send.result.completeExceptionally(MessageRejectException("buffer is full", send.message))
    }
  }

  //

  override fun connect(host: String, port: Int, identity: String): Socket {
    assertServiceState(ACCEPTING)
    var placed = false
    val result = sockets.computeIfAbsent(identity) {
      placed = true
      factory(identity, SocketInfo(host, port, SocketType.OUTBOUND), ClientSocketHandler(host, port, marker, identity, builder))
    }
    if (!placed) throw IllegalStateException("identity already registered")
    if (isServiceState(CONNECT)) result.start()
    return result
  }

  override fun disconnect(identity: String): Promise<Unit> {
    assertServiceState(ACCEPTING)
    return sockets[identity]?.shutdown() ?: Promise.completedFuture(Unit)
  }

  override fun bind(port: Int): Promise<Unit> {
    assertServiceState(ACCEPTING)
    val receiver = ServerSocketReceiver(port, marker, builder) { handler ->
      if (!isServiceState(CONNECT)) {
        logger.warn(marker, "inbound connection while shutdown")
        handler.stopEnsuring()
        handler.channel_maybe?.close()
        return@ServerSocketReceiver
      }
      val socket = sockets.computeIfAbsent(handler.identity, { 
        factory(handler.identity, SocketInfo(null, port, SocketType.INBOUND), null)
      })
      if (!socket.attachSocketHandle(handler)) {
        logger.warn(marker, "[$name] unable to attach socket handler")
        handler.stopEnsuring()
        handler.channel_maybe?.close()
      }
      else {
        socket.start()
      }
    }
    if (bindings.putIfAbsent(port, receiver) != null) {
      throw IllegalStateException("already bound to that port: $port")
    }
    if (isServiceState(CONNECT)) receiver.start()
    return receiver.start_promise
  }

  override fun unbind(port: Int): Promise<Unit> {
    assertServiceState(ACCEPTING)
    return bindings.remove(port)?.shutdown() ?: return Promise.completedFuture(null)
  }

  //

  override fun actualStart(): Promise<Unit> {
    return future {
      bindings.values.map { it.start() }.forEach { it.await() }
      sockets.values.map { it.start() }.forEach { it.await() }
      Unit
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    val start = currentTime()
    return future {
      bindings.values.map { it.shutdown() }.forEach { it.await() }
      bindings.clear()
      
      sockets.values.map { it.shutdown() }.forEach { it.await() }
      sockets.clear()
    }.whenComplete { _, th ->
      if (th == null) {
        logger.info(marker, "closed collection in ${currentTime() - start}ms")
      }
      else {
        logger.error(marker, "error while closing socket collection", th)
      }
    }
  }

  //

  private fun factory(identity: String, info: SocketInfo, socket_handler: SocketHandler?): SocketImpl {
    return SocketImpl(identity, config, info, this::socketStateHandle, marker, socket_handler, inbound_handler)
  }

  private fun socketStateHandle(socket: Socket) {
    if (!is_running) return
    logger.info(marker, "[${socket.identity}] connection state changed: ${socket.identity} -> ${socket.socket_state}")
    when (socket.socket_state) {
      SocketState.SHAKING      -> {
        routed -= socket as SocketImpl
      }
      SocketState.NORMAL       -> {
        routed += socket as SocketImpl
        drainBufferedMessages()
      }
      SocketState.DOWN         -> {
        routed -= socket as SocketImpl
        if (config.collection_remove_on_down) {
          sockets.remove(socket.identity)
          socket.shutdown()
        }
      }
      SocketState.DOWN_FINDING -> {
        routed -= socket as SocketImpl
      }
      SocketState.CLOSED       -> {
        routed -= socket as SocketImpl
        sockets.remove(socket.identity)
      }
    }
    socket_state(socket)
  }

  private fun drainBufferedMessages() {
    val pushing = LinkedList<TellableAlgorithmData<*>>()
    while (true) {
      pushing += buffered_messages.poll() ?: break
    }
    pushing.forEach { actualSendAttempt(it) }
  }
}
