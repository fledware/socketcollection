package fledware.socketcollection.internal

import fledware.socketcollection.NoResponse
import fledware.socketcollection.Promise
import io.netty.channel.ChannelDuplexHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelPromise
import io.netty.util.Attribute
import kotlinx.coroutines.experimental.CancellationException
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.io.Serializable
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

internal data class SocketSendAsk(val ask: Any, val promise: Promise<Any>)
internal data class SocketReceiveAsk(val ask: Any, val promise: Promise<Any>)
internal data class SocketSendTell(val tell: Any, val promise: Promise<Unit>)

private data class SocketInternalAsk(val ask_id: String, val ask: Any) : Serializable
private data class SocketInternalAskResponse(val ask_id: String, val response: Any) : Serializable
private data class SocketInternalAskResponseAck(val ask_id: String) : Serializable
private data class SocketInternalTell(val tell_id: String, val tell: Any) : Serializable
private data class SocketInternalTellAcknowledged(val tell_id: String) : Serializable

private data class UserTell(val request_id: String,
                            val promise: Promise<Unit>)

private data class UserAsk(val request_id: String,
                           val promise: Promise<Any>)

class DuplexAckAdapter() : ChannelDuplexHandler() {
  companion object {
    private val logger = LoggerFactory.getLogger("fledware.socketcollection.internal.protocol.ack")
  }

  private val tells = ConcurrentHashMap<String, UserTell>()
  private val asks = ConcurrentHashMap<String, UserAsk>()
  private val ask_acks = ConcurrentHashMap<String, Job>()
  private lateinit var marker: Attribute<Marker?>

  override fun channelRead(context: ChannelHandlerContext, message: Any) {
    when (message) {
      is SocketInternalAsk              -> {
        logger.trace(marker.get(), "SocketInternalAsk received: {}", message)
        // an ask request is coming in
        val result = Promise<Any>()
        val sending = SocketReceiveAsk(message.ask, result)
        result.whenComplete { response, th ->
          try {
            if (response != NoResponse) {
              ask_acks[message.ask_id] = launch(CommonPool) {
                try {
                  // just keep trying until we get an ack or the channel is closed
                  while (this.isActive && context.channel().isActive) {
                    delay(2000)
                    if (this.isActive && context.channel().isActive) {
                      // ok, lets try to send the message again
                      context.executor().submit {
                        context.writeFlushAndErrorClose(SocketInternalAskResponse(message.ask_id, th ?: response ?: Null),
                                                        logger, marker.get())
                      }
                    }
                  }
                }
                catch (ex: CancellationException) {
                  // nothing to do, the ack was successful or the channel is closing
                }
                catch (ex: Throwable) {
                  logger.warn(marker.get(), "error with reliable retries: ${ex.message}")
                }
              }
              context.writeFlushAndErrorClose(SocketInternalAskResponse(message.ask_id, th ?: response ?: Null),
                                              logger, marker.get())
            }
          }
          catch (th: Throwable) {
            logger.error(marker.get(), "could not write result", th)
            context.channel().close()
          }
        }
        super.channelRead(context, sending)
      }
      is SocketInternalAskResponse      -> {
        logger.trace(marker.get(), "SocketInternalAskResponse received: {}", message)
        // we are getting a response to an ask, so complete the promise
        val check = asks.remove(message.ask_id)
        if (check != null) {
          when (message.response) {
            is Throwable -> check.promise.completeExceptionally(message.response)
            is Null      -> check.promise.complete(null)
            else         -> check.promise.complete(message.response)
          }
        }
        context.writeFlushAndErrorClose(SocketInternalAskResponseAck(message.ask_id), logger, marker.get())
      }
      is SocketInternalAskResponseAck   -> {
        ask_acks.remove(message.ask_id)?.cancel()
      }
      is SocketInternalTell             -> {
        logger.trace(marker.get(), "SocketInternalTell received: {}", message)
        // an tell is coming in that is being asked to be acknowledged, 
        // so send the write out and send the message away
        context.writeFlushAndErrorClose(SocketInternalTellAcknowledged(message.tell_id), logger, marker.get())
        super.channelRead(context, message.tell)
      }
      is SocketInternalTellAcknowledged -> {
        logger.trace(marker.get(), "SocketInternalTellAcknowledged received: {}", message)
        // an acknowledgement is being sent back, so we need to complete
        // the promise letting the caller know
        val check = tells.remove(message.tell_id)
        if (check == null) {
          logger.warn(marker.get(), "tell response not found: $message")
        }
        else {
          check.promise.complete(null)
        }
      }
      else                              -> {
        super.channelRead(context, message)
      }
    }
  }

  override fun write(context: ChannelHandlerContext, message: Any, promise: ChannelPromise) {
    when (message) {
      is SocketSendAsk  -> {
        // we are asked to send an ask and return the result
        val request_id = UUID.randomUUID().toString()
        val ask = UserAsk(request_id, message.promise)
        asks.put(request_id, ask)
        ask.promise.whenComplete { _, _ -> asks.remove(request_id) }
        super.write(context, SocketInternalAsk(request_id, message.ask), promise)
      }
      is SocketSendTell -> {
        // we are being asked to get an ack from a tell
        val request_id = UUID.randomUUID().toString()
        val tell = UserTell(request_id, message.promise)
        tells.put(request_id, tell)
        tell.promise.whenComplete { _, _ -> tells.remove(request_id) }
        super.write(context, SocketInternalTell(request_id, message.tell), promise)
      }
      else              -> {
        super.write(context, message, promise)
      }
    }
  }

  override fun channelInactive(ctx: ChannelHandlerContext?) {
    logger.trace(marker.get(), "channel inactive")
    ask_acks.values.forEach { it.cancel() }
    ask_acks.clear()
    super.channelInactive(ctx)
  }

  override fun channelActive(ctx: ChannelHandlerContext?) {
    logger.trace(marker.get(), "channel active")
    super.channelActive(ctx)
  }

  override fun handlerAdded(context: ChannelHandlerContext) {
    marker = context.channel().attr(SocketKeys.MARKER)
  }
}
