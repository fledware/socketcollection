package fledware.socketcollection.internal

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.util.Attribute
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.io.Serializable
import java.util.concurrent.atomic.AtomicReference

sealed class PingProtocol : Serializable {
  // client -> server
  object Ping : PingProtocol()

  // server -> client in response to Ping
  object Pong : PingProtocol()

  // initiater -> other, then after is like this:
  // challenge -> hello -> helloback
  object Hello : PingProtocol()

  // hello -> helloback
  object HelloBack : PingProtocol()

  // client -> server then server -> client.
  object GoodBye : PingProtocol()
  
  // client -> server after GoodBye handshake. after this message is
  // sent, the client will close. and when the message is received by
  // the server, the server will close the connection.
  object GoodByeAck : PingProtocol()

  // server -> client, when client sends message and server doesnt know them
  object Challenge : PingProtocol()
}

private enum class PingState {
  INACTIVE,
  SHAKING,
  NORMAL
}

class InboundPingAdapter(private val config: ChannelInitConfig) : ChannelInboundHandlerAdapter() {
  companion object {
    private val logger = LoggerFactory.getLogger("fledware.socketcollection.internal.protocol.ping")
  }

  private lateinit var last_send: Attribute<Long>
  private lateinit var last_receive: Attribute<Long>
  private lateinit var is_client: Attribute<Boolean>
  private lateinit var marker: Attribute<Marker?>
  private lateinit var _context: ChannelHandlerContext
  private val state = AtomicReference<PingState>(PingState.INACTIVE)

  private val monitor = launch(CommonPool, start = CoroutineStart.LAZY) {
    while (true) {
      delay(1000L)
      if (state.get() == PingState.INACTIVE) continue
      val current = currentTime()
      if (last_receive.get() + config.socket_reconnect_timeout < current) {
        _context.close()
      }
      // if the socket is timed out, make sure its not in the routed
      // sockets and start sending hellos
      else if (last_receive.get() + config.socket_timeout < current) {
        logger.warn(marker.get(), "socket timeout: ${_context.channel().remoteAddress()}")
        when (state.get()) {
          PingState.INACTIVE -> Unit
          PingState.SHAKING  -> Unit
          PingState.NORMAL   -> {
            state.set(PingState.SHAKING)
            _context.fireChannelInactive()
            _context.writeFlushAndErrorClose(PingProtocol.Challenge, logger, marker.get())
          }
          null               -> _context.close()
        }
        if (last_send.get() + config.ping_quiet_period < current) {
          _context.writeFlushAndErrorClose(PingProtocol.Hello, logger, marker.get())
        }
      }
      // no timeouts, so check and validate the state
      else if (last_send.get() + config.ping_quiet_period < current) {
        logger.debug(marker.get(), "quiet period timeout: ${_context.channel().remoteAddress()}")
        when (state.get()) {
          PingState.INACTIVE -> Unit
          PingState.SHAKING  -> _context.writeFlushAndErrorClose(PingProtocol.Hello, logger, marker.get())
          PingState.NORMAL   -> _context.writeFlushAndErrorClose(PingProtocol.Ping, logger, marker.get())
          null               -> _context.close()
        }
      }
      else {
        logger.trace(marker.get(), "connection is ok: {}", _context.channel().remoteAddress())
      }
    }
  }

  override fun channelRead(context: ChannelHandlerContext, message: Any) {
    when (state.get()) {
      PingState.NORMAL   -> {
        when (message) {
          is PingProtocol.Ping      -> {
            context.writeFlushAndErrorClose(PingProtocol.Pong, logger, marker.get())
          }
          is PingProtocol.Pong      -> Unit
          is PingProtocol.Hello     -> {
            logger.info(marker.get(), "received hello while in valid state. sending HelloBack")
            context.writeFlushAndErrorClose(PingProtocol.HelloBack, logger, marker.get())
          }
          is PingProtocol.HelloBack -> {
            logger.info(marker.get(), "received hello while in valid state.")
          }
          is PingProtocol.GoodBye   -> {
            // should not get here
            context.close()
          }
          is PingProtocol.Challenge -> {
            // if the other side is challenging the connection, set the 
            // channel to inactive and send hello
            state.set(PingState.SHAKING)
            context.fireChannelInactive()
            context.writeFlushAndErrorClose(PingProtocol.Hello, logger, marker.get())
          }
          else                      -> {
            context.fireChannelRead(message)
          }
        }
      }
      PingState.INACTIVE -> {
        // we got a message before channel active..? close the channel, we 
        // dont know what we're doing in life
        logger.warn(marker.get(), "received message before active")
        context.close()
      }
      PingState.SHAKING  -> {
        when (message) {
          is PingProtocol.Challenge                     -> {
            logger.warn(marker.get(), "challenge received... sending hello")
            context.writeFlushAndErrorClose(PingProtocol.Hello, logger, marker.get())
          }
          is PingProtocol.Hello, is PingProtocol.HelloBack -> {
            if (message is PingProtocol.Hello) {
              context.writeFlushAndErrorClose(PingProtocol.HelloBack, logger, marker.get())
            }
            logger.debug(marker.get(), "shaking complete")
            state.set(PingState.NORMAL)
            context.fireChannelActive()
          }
          else                                          -> {
            context.writeFlushAndErrorClose(PingProtocol.Challenge, logger, marker.get())
            logger.warn(marker.get(), "challenge sent because of invalid message during shaking state: $message")
          }
        }
      }
      null               -> error("invalid null state")
    }
  }

  override fun channelActive(context: ChannelHandlerContext) {
    logger.trace(marker.get(), "channel active")
    state.set(PingState.SHAKING)
    val initiate = is_client.get()
    if (initiate == null) {
      logger.error(marker.get(), "is client is not set")
      context.channel().close()
      return
    }
    if (initiate) {
      context.writeFlushAndErrorClose(PingProtocol.Hello, logger, marker.get())
    }
  }

  override fun channelInactive(context: ChannelHandlerContext) {
    logger.trace(marker.get(), "channel inactive")
    state.set(PingState.INACTIVE)
    super.channelInactive(context)
  }

  override fun userEventTriggered(context: ChannelHandlerContext, evt: Any) {
    super.userEventTriggered(context, evt)
  }

  override fun handlerAdded(context: ChannelHandlerContext) {
    marker = context.channel().attr(SocketKeys.MARKER)
    last_send = context.channel().attr(SocketKeys.LAST_SEND)
    last_receive = context.channel().attr(SocketKeys.LAST_RECEIVE)
    is_client = context.channel().attr(SocketKeys.IS_CLIENT)
    this._context = context
    monitor.start()
    logger.trace(marker.get(), "handlerAdded: {}", context.channel().remoteAddress())
  }

  override fun handlerRemoved(context: ChannelHandlerContext) {
    logger.trace(marker.get(), "handlerRemoved: {}", context.channel().remoteAddress())
    monitor.cancel()
  }
}
