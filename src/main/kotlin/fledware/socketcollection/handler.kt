package fledware.socketcollection

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.future.await
import org.slf4j.LoggerFactory
import java.io.Closeable


/**
 * the interface for handling incoming messages.
 */
typealias InboundHandler = suspend (socket: Socket, message: Any, is_ask: Boolean) -> /*response*/Any?

/**
 * use this when you want to force no response
 */
object NoResponse

/**
 * default handler... this only logs and throws an error
 * if a message is received.
 */
object InboundHandlerDefault {
  private val logger = LoggerFactory.getLogger(InboundHandlerDefault::class.java)

  @Suppress("UNUSED_ANONYMOUS_PARAMETER")
  val instance: InboundHandler = { socket: Socket, message: Any, is_ask: Boolean ->
    logger.error("cannot handle inbound message: $message")
    throw UnsupportedOperationException("cannot handle inbound asks")
  }
}

/**
 * handler that wraps a standard inbound handler and
 * executes the messages one at a time in a suspended
 * context.
 */
class SerialInboundHandler(private val block: InboundHandler) : Closeable {

  companion object {
    private val logger = LoggerFactory.getLogger(SerialInboundHandler::class.java)
  }

  private data class InboundMessage(val socket: Socket, val message: Any, val promise: Promise<Any>?)

  val handler: InboundHandler = { socket, message, is_ask ->
    val result = if (is_ask) {
      val promise = Promise<Any>()
      runner.send(InboundMessage(socket, message, promise))
      promise.await()
    }
    else {
      runner.send(InboundMessage(socket, message, null))
      null
    }
    result
  }

  private val runner = actor<InboundMessage>(CommonPool) {
    for (message in channel) {
      try {
        val result = block(message.socket, message.message, message.promise != null)
        if (message.promise != null) message.promise.complete(result)
      }
      catch (ex: Exception) {
        if (message.promise == null) {
          logger.error("unhandled error in serial handler: $message", ex)
        }
        else {
          message.promise.complete(ex)
        }
      }
    }
  }

  override fun close() {
    runner.close()
  }
}
