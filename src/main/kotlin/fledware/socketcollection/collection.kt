package fledware.socketcollection


interface SocketCollection : Tellable, Broadcastable, SimpleService {

  val config: SocketCollectionConfig
  val name: String
  val sockets: Map<String, Socket>
  val bound_ports: Set<Int>
  override val active: List<Socket>

  fun connect(host: String, port: Int, identity: String = identities.generate()): Socket
  fun disconnect(identity: String) : Promise<Unit>
  fun bind(port: Int): Promise<Unit>
  fun unbind(port: Int): Promise<Unit>

  fun tell(message: Any,
           total_timeout: Long = config.tell_total_timeout,
           acknowledged: Boolean = config.tell_acknowledged,
           ack_retry_timeout: Long = config.tell_ack_retry_timeout)
      : Promise<Unit>

  fun <T> ask(message: Any,
              total_timeout: Long = config.ask_total_timeout,
              retry_timeout: Long = config.ask_retry_timeout)
      : Promise<T>
}
