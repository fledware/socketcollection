package fledware.socketcollection

import org.slf4j.LoggerFactory
import org.slf4j.Marker

/**
 * the main interface for sending messages
 */
interface Tellable {

  fun tell(message: Any, acknowledged: Boolean = false): Promise<Unit>
  fun <T> ask(message: Any): Promise<T>
}

/**
 * interface for broadcasting messages
 */
interface Broadcastable {

  val active: List<Tellable>

  fun broadcast(message: Any, acknowledged: Boolean = false) = active.forEach { it.tell(message, acknowledged) }
  fun <T> broadcastAsk(message: Any) = active.map { it.ask<T>(message) }
}

/**
 * shared configuration for the different parts of the system
 */
data class SocketCollectionConfig(
    /**
     * the amount of messages that can be buffered on a given socket
     */
    val socket_message_buffer: Int = -1,

    /**
     * if a socket is outbound (initiated the connection), then it
     * will always try to reconnect. this is helpful if a socket is connected
     * to a master, and even if a master leaves the cluster in a valid way
     * (like maintenance rotations and such), then the client socket will
     * still try to connect. it will only fully disconnect if the client says so.
     * this is ignored for inbound connections.
     */
    val socket_force_reconnect: Boolean = false,

    /**
     * lets the socket attempt resending a message if there is an IO error.
     * this will put the message back into the tell buffer on the socket.
     */
    val socket_allow_send_retry: Boolean = true,

    /**
     * automatically move a connection to DOWN state when its been 
     * in SHAKING state for this amount of millis
     */
    val socket_down_after_millis: Long = 10_000,

    /**
     * force the closure of a connection if the close handshake goes on for too long
     */
    val socket_close_timeout: Long = 2_000,

    /**
     * the amount of messages that can be buffered on the collection. this is
     * useful if the collection is used as a router
     */
    val collection_message_buffer: Int = -1,

    /**
     * if the write actually fails for a network problem, then that message
     * will be put back into the collection tell buffer.
     */
    val collection_allow_send_retry: Boolean = true,

    /**
     * if a socket goes to a DOWN state, then the socket is automatically 
     * removed from the socket pool. this is helpful if the state of 
     * the connection is not important. be careful setting this to false 
     * though, because it means you will need to clean the connections
     * yourself.
     * also, because the collections is actually the one cleaning the
     * sockets, if will override socket_force_reconnect.
     */
    val collection_remove_on_down: Boolean = true,
    
    /**
     * the amount of time the entire request should take before a timeout
     * exception is used to complete the promise
     */
    val tell_total_timeout: Long = 5000L,
    
    /**
     * this requests that the remove server sends an ack when
     * the message is received
     */
    val tell_acknowledged: Boolean = true,
    
    /**
     * the amount of time before an acknowledged tell is retried.
     * NOTE: this is mostly useful for collections because a retry will
     * most likely go to another socket.
     * NOTE: this is in conjunction with send_total_timeout in that
     * the tellAck will be completed with an exception if a timeout
     * is reached.
     * NOTE: setting this to 0 or less will not allow retries. do not
     * retry messages that are not idempotent 
     */
    val tell_ack_retry_timeout: Long = (tell_total_timeout * 0.80).toLong(),
    
    /**
     * the timeout for the entire ask request to be finished before a
     * timeout exception is used to complete the promise
     */
    val ask_total_timeout: Long = 8000L,
    
    /**
     * the timeout used before an ask is retried.
     * NOTE: this is mostly useful for collections because a retry will
     * most likely go to another socket.
     * NOTE: this is in conjunction with send_total_timeout in that
     * the tellAck will be completed with an exception if a timeout
     * is reached.
     * NOTE: setting this to 0 or less will not allow retries. do not
     * retry messages that are not idempotent
     */
    val ask_retry_timeout: Long = (ask_total_timeout * 0.75).toLong()
)

/**
 * how do we want a socket to start? this is only relevant
 * for outbound connections. this lets us make a socket
 * and not actually start attempting connections until
 * start is called.
 */
enum class SimpleServiceStart {
  LAZY,
  IMMEDIATE
}

/**
 * the state of a service. if a service is started with IMMEDIATE
 * then the service will call start immediately. else you have
 * to call start at a later time yourself
 */
enum class SimpleServiceState {
  NEW,
  STARTING,
  RUNNING,
  CLOSING,
  CLOSED
}

interface SimpleService {
  val service_state: SimpleServiceState
  val start_promise: Promise<Unit>
  val shutdown_promise: Promise<Unit>
  val is_running: Boolean get() = service_state == SimpleServiceState.RUNNING

  fun start(): Promise<Unit>
  fun shutdown(): Promise<Unit>
  fun assertServiceRunning() = run { if (!is_running) throw IllegalStateException("not running: $service_state") }
  fun isServiceState(allowed: Set<SimpleServiceState>) = service_state in allowed
  fun assertServiceState(allowed: Set<SimpleServiceState>) {
    if (service_state !in allowed) {
      throw IllegalStateException("illegal state: allowed($allowed) actual($service_state)")
    }
  }
}

abstract class SimpleServiceImpl(private val marker: Marker?) : SimpleService {

  private val logger = LoggerFactory.getLogger(javaClass)
  protected open fun actualStart(): Promise<Unit> = promiseUnitComplete()
  protected open fun actualShutdown(): Promise<Unit> = promiseUnitComplete()

  @Volatile
  override final var service_state: SimpleServiceState = SimpleServiceState.NEW
    private set
  override final val start_promise = Promise<Unit>()
  override final val shutdown_promise = Promise<Unit>()

  init {
    start_promise.whenComplete { _, th -> handleStartComplete(th) }
    shutdown_promise.whenComplete { _, th -> handleShutdownComplete(th) }
  }

  @Synchronized
  private fun handleStartComplete(th: Throwable?) {
    if (th != null) {
      logger.error(marker, "failed to start", th)
      shutdown()
    }
    else if (service_state == SimpleServiceState.STARTING) {
      service_state = SimpleServiceState.RUNNING
    }
  }
  
  @Synchronized
  private fun handleShutdownComplete(th: Throwable?) {
    if (th != null) {
      logger.error(marker, "failed to shutdown", th)
    }
    service_state = SimpleServiceState.CLOSED
  }

  @Synchronized
  override final fun start(): Promise<Unit> {
    when (service_state) {
      SimpleServiceState.NEW      -> {
        logger.debug(marker, "starting service")
        service_state = SimpleServiceState.STARTING
        actualStart().proxyFinished(start_promise)
      }
      SimpleServiceState.STARTING -> {
        logger.debug(marker, "service already starting and start() was called")
      }
      SimpleServiceState.RUNNING  -> {
        logger.debug(marker, "service already running and start() was called")
      }
      SimpleServiceState.CLOSING  -> throw IllegalStateException("service is closing")
      SimpleServiceState.CLOSED   -> throw IllegalStateException("service is closed")
    }
    return start_promise
  }

  @Synchronized
  override final fun shutdown(): Promise<Unit> {
    when (service_state) {
      SimpleServiceState.NEW      -> {
        logger.debug(marker, "service is new, just closing..")
        service_state = SimpleServiceState.CLOSED
        start_promise.cancel(true)
        shutdown_promise.complete(null)
      }
      SimpleServiceState.STARTING -> {
        logger.debug(marker, "service is starting, cancelling and closing..")
        service_state = SimpleServiceState.CLOSING
        start_promise.cancel(true)
        actualShutdown().proxyFinished(shutdown_promise)
      }
      SimpleServiceState.RUNNING  -> {
        logger.debug(marker, "service is running, closing..")
        service_state = SimpleServiceState.CLOSING
        actualShutdown().proxyFinished(shutdown_promise)
      }
      SimpleServiceState.CLOSING  -> {
        logger.debug(marker, "service is already closing")
      }
      SimpleServiceState.CLOSED   -> {
        logger.debug(marker, "service is already closed..")
      }
    }
    return shutdown_promise
  }
}
