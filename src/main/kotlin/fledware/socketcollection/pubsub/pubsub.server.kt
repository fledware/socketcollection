package fledware.socketcollection.pubsub

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.factory
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.future.future
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.Collections
import java.util.concurrent.ConcurrentHashMap


/**
 * the server part for pubsub
 */
class PubSubServer(val bind: Int,
                   val name: String,
                   private val marker: Marker?,
                   builder: ChannelInitFactory = ChannelInitFactoryDefault()) : SimpleServiceImpl(marker) {
  companion object {
    private val logger = LoggerFactory.getLogger(PubSubServer::class.java)
    private val KEY = "subs"
    private fun <T> set() = Collections.newSetFromMap(ConcurrentHashMap<T, Boolean>())
  }

  val collection = factory.collectionAsConfigured("$name-publisher",
                                                  marker = marker,
                                                  start_strategy = SimpleServiceStart.LAZY,
                                                  config = SocketCollectionConfig(
                                                      socket_message_buffer = 100,
                                                      socket_force_reconnect = false,
                                                      socket_allow_send_retry = true,
                                                      collection_message_buffer = -1,
                                                      collection_allow_send_retry = true
                                                  ),
                                                  builder = builder,
                                                  handler = { s, m, i -> handleInbound(s, m, i) }).also { it.bind(bind) }
  
  val subscriptions = ConcurrentHashMap<String, MutableSet<Socket>>()

  override fun actualStart(): Promise<Unit> {
    return future {
      collection.start().await()
      Unit
    }
  }

  override fun actualShutdown(): Promise<Unit> {
    return collection.shutdown()
  }

  suspend fun handleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    when (message) {
      is PubSubProtocol.Publish      -> {
        logger.info(marker, "publish received: {}", message)
        val sockets = LinkedHashSet<Socket>()
        message.topics.forEach { topic ->
          val check = subscriptions[topic]
          if (check != null) sockets += check
        }
        // TODO: read if this is an ask and to a tellAck here instead, then return the count/total
        sockets.forEach { it.tell(message) }
      }
      is PubSubProtocol.GetAllTopics -> {
        return subscriptions.keys.toSet()
      }
      is PubSubProtocol.Resubscribe  -> {
        logger.info(marker, "resubscribe received $message")
        var adding = emptySet<String>()
        var removing = emptySet<String>()
        socket.subscriptions { current ->
          adding = (message.topics - current)
          removing = (current - message.topics)
        }
        socket.subscribe(adding, false)
        socket.unsubscribe(removing, false)
        collection.broadcast(PubSubProtocol.Resubscribe(subscriptions.keys.toSet()))
      }
      is PubSubProtocol.Subscribe    -> {
        logger.info(marker, "subscribe received: $message")
        socket.subscribe(message.topics, true)
      }
      is PubSubProtocol.Unsubscribe  -> {
        logger.info(marker, "unsubscribe received: $message")
        socket.unsubscribe(message.topics, true)
      }
      else                           -> {
        logger.warn(marker, "unhandled message [1]: $message")
      }
    }
    return null
  }

  private fun Socket.subscriptions(block: (MutableSet<String>) -> Unit) {
    data.compute(KEY) { _, check ->
      @Suppress("UNCHECKED_CAST")
      val subs = check as? MutableSet<String> ?: set()
      block(subs)
      return@compute if (subs.isNotEmpty()) subs else null
    }
  }

  private fun Socket.subscribe(topics: Set<String>, broadcast: Boolean) {
    subscriptions { it += topics }
    val additions = LinkedHashSet<String>()
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, check ->
        val set = check ?: set()
        set.add(this)
        if (check == null) additions += topic
        return@compute set
      }
    }
    if (broadcast && additions.isNotEmpty()) {
      collection.broadcast(PubSubProtocol.Subscribe(additions))
    }
  }

  private fun Socket.unsubscribe(topics: Set<String>, broadcast: Boolean) {
    subscriptions { it -= topics }
    val removals = LinkedHashSet<String>()
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, set ->
        if (set == null) return@compute null
        set -= this
        if (set.isEmpty()) {
          removals += topic
          return@compute null
        }
        return@compute set
      }
      kotlin.Unit
    }
    if (broadcast && removals.isNotEmpty()) {
      collection.broadcast(PubSubProtocol.Unsubscribe(removals))
    }
  }
}
