package fledware.socketcollection.pubsub

import fledware.socketcollection.Promise
import fledware.socketcollection.promiseUnitComplete
import java.util.concurrent.ConcurrentHashMap

/**
 * not part of a distributed system, but its useful
 * and can help with unit testing for mocking an such.
 */
class PubSubBus : PubSubClient {
  private val subscriptions = ConcurrentHashMap<String, MutableSet<EventHandler>>()

  fun clearSubscriptions() {
    subscriptions.clear()
  }

  override fun publish(topics: Set<String>, event: Any) {
    val sending = LinkedHashSet<EventHandler>()
    topics.forEach { topic ->
      val check = subscriptions[topic]
      if (check != null) sending += check
    }
    sending.forEach { it(topics, event) }
  }

  override fun subscribe(topics: Set<String>, handler: EventHandler): Promise<Unit> {
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, check ->
        val set = check ?: ConcurrentHashMap.newKeySet<EventHandler>()
        set += handler
        return@compute set
      }
    }
    return promiseUnitComplete()
  }

  override fun unsubscribe(topics: Set<String>, handler: EventHandler): Promise<Unit> {
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, check ->
        val set = check ?: return@compute null
        set -= handler
        return@compute if (set.isNotEmpty()) set else null
      }
    }
    return promiseUnitComplete()
  }
}
