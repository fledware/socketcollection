package fledware.socketcollection.pubsub

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.identities
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import org.slf4j.Marker
import java.io.Serializable


/**
 * the handler used for receiving messages. we can send a message 
 * with multiple topics attached to it. this is important if we
 * are interested in a topic hierarchy. for instance, if we publish
 * to these topics:
 * - service1
 * - service1-action1
 * - service1-action1-data1
 * - service1-action1-data2
 * - service1-action2
 * - service1-action2-data1
 * - service1-action2-data2
 * 
 * and when you publish, you would publish to each topic in the
 * given hierarchy. for instance, if you want have the class:
 * data class Service1Action1(val data: String)
 * data class Service1Action2(val data: String)
 * 
 * then when you publish an object Service1Action1(data="somedata"), these
 * would be the topics:
 * - service1
 * - service1-action1
 * - service1-action1-somedata
 * 
 * this would allow people to listen to all events for service1, 
 * or just whenever action1 happens. but also you can get a topic
 * only if service1 sends an event of action1 with specific pieces of
 * data attached to it, in this case "somedata".
 */
typealias EventHandler = (topics: Set<String>, event: Any) -> Unit

/**
 * client for pubsub
 */
interface PubSubClient {
  fun publish(topics: Set<String>, event: Any)
  fun subscribe(topics: Set<String>, handler: EventHandler): Promise<Unit>
  fun unsubscribe(topics: Set<String>, handler: EventHandler): Promise<Unit>
}

/**
 * the internal protocol used for pubsub
 */
sealed class PubSubProtocol : Serializable {

  // client -> collection -> listening clients
  data class Publish(val topics: Set<String>,
                     val event: Any)
    : PubSubProtocol()

  // client -> collection -> listening clients
  // use to let the collection know exactly the list that should
  // be used for subscriptions on the given client
  data class Resubscribe(val topics: Set<String>)
    : PubSubProtocol()

  // client -> collection -> new subs to clients
  // lets collection know to add the given topics to the collections
  // server socket
  data class Subscribe(val topics: Set<String>)
    : PubSubProtocol()

  // client -> collection -> remove old subs from clients
  data class Unsubscribe(val topics: Set<String>)
    : PubSubProtocol()

  // client -> collection: returns Set<String>
  // 
  object GetAllTopics
    : PubSubProtocol()
}

object pubsub_factory {
  
  fun server(bind: Int,
             name: String = identities.generate(),
             marker: Marker? = null,
             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
             builder: ChannelInitFactory = ChannelInitFactoryDefault()): PubSubServer {
    val result = PubSubServer(bind, name, marker, builder)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
  
  fun client(host: String,
             port: Int,
             name: String = identities.generate(),
             marker: Marker? = null,
             start_strategy: SimpleServiceStart = SimpleServiceStart.IMMEDIATE,
             builder: ChannelInitFactory = ChannelInitFactoryDefault()): PubSubClientImpl {
    val result = PubSubClientImpl(host, port, name, marker, builder)
    if (start_strategy == SimpleServiceStart.IMMEDIATE) result.start()
    return result
  }
  
  fun localBus(): PubSubBus {
    return PubSubBus()
  }
}
