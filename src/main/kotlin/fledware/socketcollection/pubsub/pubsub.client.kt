package fledware.socketcollection.pubsub

import fledware.socketcollection.Promise
import fledware.socketcollection.SimpleServiceImpl
import fledware.socketcollection.SimpleServiceStart
import fledware.socketcollection.Socket
import fledware.socketcollection.SocketCollectionConfig
import fledware.socketcollection.SocketState
import fledware.socketcollection.factory
import fledware.socketcollection.internal.ChannelInitFactory
import fledware.socketcollection.internal.ChannelInitFactoryDefault
import fledware.socketcollection.promiseUnitComplete
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import java.util.concurrent.ConcurrentHashMap


// ========================================================
// client implementation 
// ========================================================

class PubSubClientImpl(private val host: String,
                       private val port: Int,
                       val name: String,
                       private val marker: Marker?,
                       builder: ChannelInitFactory = ChannelInitFactoryDefault()) : SimpleServiceImpl(marker),
                                                                                    PubSubClient {
  companion object {
    private val logger = LoggerFactory.getLogger(PubSubClientImpl::class.java)
  }

  val server = factory.socketCreateStandalone(host, port,
                                              marker = marker,
                                              start_strategy = SimpleServiceStart.LAZY,
                                              config = SocketCollectionConfig(
                                                  socket_message_buffer = 1000,
                                                  socket_force_reconnect = true,
                                                  socket_allow_send_retry = true
                                              ),
                                              builder = builder,
                                              socket_state = this::socketStateChange,
                                              handler = { s, m, i -> handleInbound(s, m, i) })
  val subscriptions = ConcurrentHashMap<String, MutableSet<EventHandler>>()
  val cluster_listens = ConcurrentHashMap.newKeySet<String>()!!

  override fun actualStart(): Promise<Unit> {
    return server.start()
  }

  override fun actualShutdown(): Promise<Unit> {
    return server.shutdown()
  }

  private fun socketStateChange(socket: Socket) {
    when (socket.socket_state) {
      SocketState.SHAKING -> {
        logger.warn(marker, "pubsub server connection is shaking")
      }
      SocketState.NORMAL  -> {
        logger.warn(marker, "pubsub server is up")
        socket.tell(PubSubProtocol.Resubscribe(subscriptions.keys.toSet()))
      }
      SocketState.CLOSED  -> {
        logger.error(marker, "pubsub server is going away... reconnecting anyways")
      }
      SocketState.DOWN    -> {
        logger.error(marker, "pubsub server is considered down")
      }
      SocketState.DOWN_FINDING -> {
        logger.error(marker, "pubsub server left and we are trying to reconnect when it comes back")
      }
    }
  }

  suspend fun handleInbound(socket: Socket, message: Any, is_ask: Boolean): Any? {
    when (message) {
      is PubSubProtocol.Publish      -> {
        logger.debug(marker, "publish received: {}", message.event)
        val handlers = LinkedHashSet<EventHandler>()
        message.topics.forEach { topic ->
          val check = subscriptions[topic]
          if (check != null) handlers += check
        }
        if (handlers.isEmpty()) {
          // we should do filtering on the server
          logger.warn(marker, "no handlers for publish: $message")
        }
        handlers.forEach { it(message.topics, message.event) }
      }
      is PubSubProtocol.GetAllTopics -> {
        return cluster_listens.toSet()
      }
      is PubSubProtocol.Resubscribe  -> {
        logger.info(marker, "resubscribe received")
        cluster_listens += message.topics
        val needs_subbed = LinkedHashSet<String>()
        val it = cluster_listens.iterator()
        while (it.hasNext()) {
          val topic = it.next()
          if (!message.topics.contains(topic)) {
            // this means we need to remove the topic from cluster_listens..
            // but first lets check that we dont actually care about it, and if we
            // do then we need to let the collection know by subscribing to it again
            if (subscriptions.containsKey(topic)) {
              // we have a problem
              needs_subbed += topic
            }
            else {
              cluster_listens -= topic
            }
          }
        }
        if (needs_subbed.isNotEmpty()) {
          logger.warn(marker, "the collection asked us to unsub from a topic we care about: $needs_subbed")
          server.tell(PubSubProtocol.Subscribe(needs_subbed))
        }
      }
      is PubSubProtocol.Subscribe    -> {
        logger.info(marker, "subscribe received: $message")
        cluster_listens += message.topics
      }
      is PubSubProtocol.Unsubscribe  -> {
        logger.info(marker, "unsubscribe received: $message")
        cluster_listens -= message.topics
        // make sure that we are not removing one that we care about
        val we_care = message.topics.any { subscriptions.containsKey(it) }
        if (we_care) {
          logger.warn(marker, "server told us to unsub to a topic we care about: $message")
          socket.tell(PubSubProtocol.Resubscribe(subscriptions.keys.toSet()))
        }
      }
      else                           -> {
        logger.warn(marker, "unhandled message [1]: $message")
      }
    }
    return null
  }

  override fun publish(topics: Set<String>, event: Any) {
    val sending = topics.any { cluster_listens.contains(it) }
    if (sending) {
      server.tell(PubSubProtocol.Publish(topics, event))
    }
    else {
      logger.debug(marker, "not sending message: $event")
    }
  }

  override fun subscribe(topics: Set<String>, handler: EventHandler): Promise<Unit> {
    var additions: MutableSet<String>? = null
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, check ->
        val handlers = check ?: ConcurrentHashMap.newKeySet<EventHandler>()
        handlers += handler
        if (check == null) {
          if (additions == null) additions = LinkedHashSet()
          additions!! += topic
        }
        return@compute handlers
      }
    }
    if (additions == null) {
      return promiseUnitComplete()
    }
    return server.tell(PubSubProtocol.Subscribe(additions!!), acknowledged = true)
  }

  override fun unsubscribe(topics: Set<String>, handler: EventHandler): Promise<Unit> {
    var removals: MutableSet<String>? = null
    topics.forEach { topic ->
      subscriptions.compute(topic) { _, check ->
        if (check == null) return@compute null
        check -= handler
        if (check.isEmpty()) {
          if (removals == null) removals = LinkedHashSet()
          removals!! += topic
          return@compute null
        }
        return@compute check
      }
    }
    if (removals == null) {
      return promiseUnitComplete()
    }
    return server.tell(PubSubProtocol.Unsubscribe(removals!!), acknowledged = true)
  }
}
