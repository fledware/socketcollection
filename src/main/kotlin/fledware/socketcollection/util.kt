package fledware.socketcollection

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.slf4j.MarkerFactory
import java.util.UUID
import java.util.concurrent.CancellationException
import java.util.concurrent.CompletableFuture


// ========================================================
// common exceptions
// ========================================================

class MessageRejectException(message: String, val sending: Any) : Exception(message)
class MessageTimeoutException(message: String, val sending: Any) : Exception(message)


// ========================================================
// promise related utilities
// ========================================================

private val logger = LoggerFactory.getLogger("fledware.socketcollection.promise")
typealias Promise<T> = CompletableFuture<T>
private val COMPLETED = Promise.completedFuture<Unit>(null)

@Suppress("UNCHECKED_CAST")
fun <T> promiseNullComplete(): Promise<T> = COMPLETED as Promise<T>

@Suppress("UNCHECKED_CAST")
fun promiseUnitComplete(): Promise<Unit> = COMPLETED as Promise<Unit>

fun <T> promiseFailed(th: Throwable): Promise<T> = Promise<T>().also { it.completeExceptionally(th) }

fun <T> Promise<T>.whenException(block: (Throwable) -> Unit): Promise<T> = whenComplete { _, th -> if (th != null) block(th) }

fun <T> Promise<T>.proxyFinished(promise: Promise<T>): Promise<T> {
  this.whenComplete { result, th ->
    when (th) {
      null -> promise.complete(result)
      else -> promise.completeExceptionally(th)
    }
  }
  return promise
}

fun <T> Promise<T>.runIfNotComplete(timeout_ms: Long, block: (Promise<T>) -> Unit) {
  val job = launch(CommonPool) {
    try {
      delay(timeout_ms)
      if (!this@runIfNotComplete.isDone) block(this@runIfNotComplete)
    }
    catch (ex: CancellationException) {
      // nothing to do..
    }
    catch (ex: Exception) {
      logger.error("error while runIfNotComplete", ex)
    }
  }
  this.whenComplete { _, _ -> job.cancel() }
}

fun <T> Promise<T>.withTimeout(timeout_ms: Long, message: String = ""): Promise<T> {
  val result = Promise<T>()
  this.proxyFinished(result)
  result.runIfNotComplete(timeout_ms) {
    result.completeExceptionally(java.util.concurrent.TimeoutException(message))
  }
  return result
}


// ========================================================
// identity generation. change the generate method
// to change the default way identities are generated
// ========================================================

object identities {
  var generate: () -> String = { UUID.randomUUID().toString() }
}

fun Marker?.extend(name: String): Marker? {
  if (this == null) return null
  return MarkerFactory.getMarker("${this.name}-$name")
}
