package fledware.socketcollection

import fledware.socketcollection.internal.currentTime
import org.junit.After
import org.slf4j.LoggerFactory
import java.io.IOException
import java.net.Socket

val LOCALHOST = "127.0.0.1"

object PortHelper {
  private val logger = LoggerFactory.getLogger(PortHelper::class.java)
  private val port_max = 50_000
  private val port_min = 25_000
  private val attempt_max = 1000
  private var port = port_min
  private fun isPortFree(port: Int): Boolean {
    try {
      Socket("localhost", port).use { _ -> return false }
    }
    catch (ignored: IOException) {
      return true
    }
  }

  @Synchronized
  fun nextFreePort(): Int {
    var attempts = 0
    while (true) {
      port++
      if (port >= port_max) port = port_min
      val check = port
      if (isPortFree(check)) {
        logger.info("giving out port: $check")
        return check
      }
      attempts++
      if (attempts > attempt_max) {
        throw IllegalStateException("couldnt find a valid port after $attempts checks")
      }
    }
  }
}

fun nextPort() = PortHelper.nextFreePort()

fun waitUntil(timeout: Long = 5000, sleep: Long = 200, block: () -> Boolean) {
  val end_at = currentTime() + timeout
  while (currentTime() < end_at) {
    if (block()) return
    Thread.sleep(sleep)
  }
  if (!block()) throw IllegalStateException("timeout in wait block")
}

open class CleanHelper {
  val services = ArrayList<SimpleService>()
  @After
  fun after() {
    println("====================================================== [shutdown]")
    services.map { it.shutdown() }.forEach { it.join() }
    println("====================================================== [shutdown-finished]")
  }
  
  fun startAllServices() {
    println("====================================================== [start]")
    services.map { it.start() }.forEach {
      it.withTimeout(6000).join()
    }
    println("====================================================== [start-finished]")
  }

  fun <T : SimpleService> register(service: T): T {
    services += service
    return service
  }
}

val dump_socket = object: fledware.socketcollection.Socket {
  override val identity: String
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val info: SocketInfo
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val config: SocketCollectionConfig
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val data: MutableMap<String, Any>
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val socket_state: SocketState
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

  override fun waitForState(state: SocketState): Promise<Unit> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun <T> ask(message: Any): Promise<T> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun tell(message: Any, total_timeout: Long, acknowledged: Boolean, ack_retry_timeout: Long): Promise<Unit> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun <T> ask(message: Any, total_timeout: Long, retry_timeout: Long): Promise<T> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun tell(message: Any, acknowledged: Boolean): Promise<Unit> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override val service_state: SimpleServiceState
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val start_promise: Promise<Unit>
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
  override val shutdown_promise: Promise<Unit>
    get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

  override fun start(): Promise<Unit> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun shutdown(): Promise<Unit> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}
