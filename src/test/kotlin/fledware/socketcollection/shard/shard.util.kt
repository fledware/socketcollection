package fledware.socketcollection.shard

import fledware.socketcollection.CleanHelper
import fledware.socketcollection.LOCALHOST
import fledware.socketcollection.Promise
import fledware.socketcollection.Socket
import fledware.socketcollection.nextPort
import fledware.socketcollection.pubsub.PubSubClient
import fledware.socketcollection.pubsub.pubsub_factory
import fledware.socketcollection.waitUntil
import org.slf4j.LoggerFactory
import org.slf4j.MarkerFactory.getMarker
import java.io.Serializable


fun createShardConfig(shard_name: String) = ShardConfiguration(
    shard_name = shard_name,
    master_bind_shard = nextPort(),
    master_bind_world = nextPort(),
    metric_delay_millis = 500,
    validation_delay = 750
)

fun createShardSeed(count: Int = 20): () -> Promise<Set<String>> {
  return {
    val shards = (1..count).mapTo(LinkedHashSet()) { "shard-$it" }
    Promise.completedFuture(shards)
  }
}

data class ShardClusterData(val master: ShardMaster,
                            val nodes: List<ShardNode>,
                            val client: ShardClient,
                            val kill: Promise<Unit>,
                            val pubsub: PubSubClient,
                            val config: ShardConfiguration)

fun CleanHelper.createShardCluster(config: ShardConfiguration,
                                   marker_prefix: String = "",
                                   node_count: Int = 2,
                                   shard_count: Int = 20,
                                   start_wait_timeout: Long = 10_000): ShardClusterData {
  println("====================================================== [shard-start]")
  val kill = Promise<Unit>()
  val pubsub = pubsub_factory.localBus()
  val master = register(shard_factory.master(config, pubsub, createShardSeed(shard_count), marker = getMarker("$marker_prefix-master")))
  val nodes = (1..node_count).map {
    register(shard_factory.node(config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("$marker_prefix-node$it")))
  }
  val client = register(shard_factory.client(config, pubsub, getMarker("$marker_prefix-client")))
  startAllServices()

  if (node_count > 0) {
    waitUntil(timeout = start_wait_timeout) { isNodesBalanced(shard_count, config.balancing_threshold) }
  }

  println("====================================================== [shard-start-finished]")
  return ShardClusterData(master, nodes, client, kill, pubsub, config)
}

fun CleanHelper.isNodesBalanced(shard_count: Int, balancing_threshold: Int, print_nodes: Boolean = false): Boolean {
  val nodes = services.filterIsInstance(ShardNode::class.java).filter { it.is_running }
  val master = services.filterIsInstance(ShardMaster::class.java).find { it.is_running }
  if (print_nodes) {
    val nodes_printing = services.filterIsInstance(ShardNode::class.java).filter { it.is_running || it.shard_ids.isNotEmpty() }
    println("[is balanced] total nodes: ${nodes_printing.size}")
    for (node in nodes_printing) {
      val master_print = if (master != null) {
        "${master.shard_collection.sockets[node.identifier]?.data?.node_data?.shards?.sorted()}"
      }
      else {
        "no master"
      }
      println("[is balanced] master ${node.marker?.name ?: node.identifier} -> $master_print")
      println("[is balanced] node   ${node.marker?.name ?: node.identifier} -> ${node.shard_ids.sorted()}")
    }
    println("[is balanced] total shards: ${nodes.sumBy { it.shard_ids.size }}")
  }
  val count = nodes.sumBy { it.shard_ids.size }
  if (count != shard_count) {
    println("[is balanced] invalid shard count: count($count) != shard_count($shard_count)")
    return false
  }
  val first = nodes[0]
  if (!nodes.all { Math.abs(first.shard_ids.size - it.shard_ids.size) <= balancing_threshold }) {
    println("[is balanced] invalid shard balance")
    return false
  }
  if (!nodes.all { it.shard_ids == master?.shard_collection?.sockets?.get(it.identifier)?.data?.node_data?.shards }) {
    println("[is balanced] shards on nodes dont match master")
  }
  return true
}


sealed class TestShardProtocol : Serializable {

  object MessageCount
    : TestShardProtocol()

  data class HelloThere(val name: String)
    : TestShardProtocol()

  data class HelloThereBack(val greetings: String)
    : TestShardProtocol()

  data class FinishInTime(val time: Long)
    : TestShardProtocol()
}

class TestShard(override val shard_id: String) : Shard {
  companion object {
    private val logger = LoggerFactory.getLogger(TestShard::class.java)
  }

  override val metric: Int = 1

  private var messages = 0

  suspend override fun receive(socket: Socket, message: Any, is_ask: Boolean): Any? {
    messages++
    return when (message) {
      is TestShardProtocol.MessageCount -> messages
      is TestShardProtocol.HelloThere   -> TestShardProtocol.HelloThereBack("hello ${message.name} from $shard_id")
      else                              -> throw IllegalArgumentException("unhandled message ($shard_id)")
    }
  }

  suspend override fun start() {
//    actor.start()
  }

  suspend override fun close() {
//    actor.close()
  }
}
