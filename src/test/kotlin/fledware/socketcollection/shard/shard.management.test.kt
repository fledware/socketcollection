package fledware.socketcollection.shard

import fledware.socketcollection.CleanHelper
import fledware.socketcollection.waitUntil
import org.junit.Assert
import org.junit.Test
import org.slf4j.MarkerFactory.getMarker


class ShardManagementTest : CleanHelper() {

  @Test
  fun testEnsureNewShard() {
    val shard_config = createShardConfig("testEnsureNewShard")
    val shard_data = createShardCluster(shard_config, "sm1")
    val client = shard_data.client

    println("================================================================ [1]")
    client.shardEnsure("shard-123").join()
    val result = client.ask<TestShardProtocol.HelloThereBack>("shard-123", TestShardProtocol.HelloThere("world"))
    Assert.assertEquals("hello world from shard-123", result.join().greetings)
    println("================================================================ [2]")
  }

  @Test
  fun testEnsure50Shards() {
    val shard_config = createShardConfig("testEnsure50Shards")
    val shard_data = createShardCluster(shard_config, "ms2")
    val client = shard_data.client

    println("================================================================ [1]")
    (11..60)
        .map { it to client.shardEnsure("shard-100$it") }
        .forEach {
          try {
            it.second.join()
          }
          catch (ex: Exception) {
            println("bad shard: ${it.first}")
            throw ex
          }
        }
    (11..60)
        .map { it to client.ask<TestShardProtocol.HelloThereBack>("shard-100$it", TestShardProtocol.HelloThere("world")) }
        .forEach { Assert.assertEquals("hello world from shard-100${it.first}", it.second.join().greetings) }
    println("================================================================ [2]")
  }
  
  @Test
  fun testDestroyShard() {
    val shard_config = createShardConfig("testDeleteShard")
    val shard_data = createShardCluster(shard_config, "sm3")
    val client = shard_data.client

    println("================================================================ [1]")
    client.shardDestroy("shard-15").join()
    waitUntil { !shard_data.master.shards.containsKey("shard-15") }
    println("================================================================ [2]")
  }

  @Test
  fun testEnsureAndDelete50Shards() {
    val shard_config = createShardConfig("testEnsureAndDelete50Shards")
    val shard_data = createShardCluster(shard_config, "ms4")
    val client = shard_data.client

    println("================================================================ [1]")
    (11..60)
        .map { it to client.shardEnsure("shard-100$it") }
        .forEach {
          try {
            it.second.join()
          }
          catch (ex: Exception) {
            println("bad shard: ${it.first}")
            throw ex
          }
        }
    waitUntil { isNodesBalanced(70, shard_config.balancing_threshold, true) }
    (11..60)
        .map { it to client.ask<TestShardProtocol.HelloThereBack>("shard-100$it", TestShardProtocol.HelloThere("world")) }
        .forEach { Assert.assertEquals("hello world from shard-100${it.first}", it.second.join().greetings) }
    println("================================================================ [2]")
    (11..60)
        .map { client.shardDestroy("shard-100$it") }
        .forEach { it.join() }
    (11..60).forEach {
      waitUntil { !shard_data.master.shards.containsKey("shard-100$it") }
    }
    waitUntil { isNodesBalanced(20, shard_config.balancing_threshold, true) }
    println("================================================================ [3]")
  }

  @Test
  fun testEnsureSerialShards() {
    val shard_config = createShardConfig("testEnsureSerialShards")
    val shard_data = createShardCluster(shard_config, "ms5")
    
    (101..200)
        .forEach {
          println("================================================================ [$it]")
          val client = register(shard_factory.client(shard_config, shard_data.pubsub, getMarker("ms5-client-$it")))
          client.start_promise.join()
          client.shardEnsure("shard-200$it").join()
          val request = client.ask<TestShardProtocol.HelloThereBack>("shard-200$it", TestShardProtocol.HelloThere("world"))
          Assert.assertEquals("hello world from shard-200$it", request.join().greetings)
          client.shutdown().join()
        }

    println("================================================================ [all-1]")
    val client = shard_data.client
    (101..200)
        .map { it to client.ask<TestShardProtocol.HelloThereBack>("shard-200$it", TestShardProtocol.HelloThere("world")) }
        .forEach { Assert.assertEquals("hello world from shard-200${it.first}", it.second.join().greetings) }
    println("================================================================ [all-2]")
  }

}
