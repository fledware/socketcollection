package fledware.socketcollection.shard

import fledware.socketcollection.CleanHelper
import fledware.socketcollection.withTimeout
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.slf4j.MarkerFactory.getMarker
import java.util.concurrent.CompletionException
import kotlin.system.measureTimeMillis

class ShardClientTest : CleanHelper() {

  @Test
  fun testHappyPath() {
    val shard_config = createShardConfig("testHappyPath")
    val shard_data = createShardCluster(shard_config, "sc1")
    val client = shard_data.client

    println("================================================================ [1]")
    val result = client.ask<TestShardProtocol.HelloThereBack>("shard-12", TestShardProtocol.HelloThere("world"))
    val check = result.join()
    Assert.assertNotNull(check)
    Assert.assertEquals("hello world from shard-12", check.greetings)
    println("================================================================ [2]")
  }

  @Test
  fun testIllegalMessage() {
    val shard_config = createShardConfig("testIllegalMessage")
    val shard_data = createShardCluster(shard_config, "sc2")
    val client = shard_data.client

    println("================================================================ [1]")
    val result = client.ask<String>("shard-12", "some-unexpected-message")
    try {
      println(result.join())
      Assert.fail()
    }
    catch (ex: CompletionException) {
      val cause = ex.cause
      println("exception: $ex")
      Assert.assertTrue(cause is IllegalArgumentException)
      Assert.assertEquals("unhandled message (shard-12)", cause!!.message)
    }
    println("================================================================ [2]")
  }

  @Test
  fun testShardNotFound() {
    val shard_config = createShardConfig("testShardNotFound")
    val shard_data = createShardCluster(shard_config, "sc3")
    val client = shard_data.client

    println("================================================================ [1]")
    val result = client.ask<String>("shard-123", "some-unexpected-message")
    try {
      println(result.join())
      Assert.fail()
    }
    catch (ex: CompletionException) {
      val cause = ex.cause as ShardNotFoundException
      Assert.assertEquals("shard-123", cause.shard_id)
      Assert.assertEquals(false, cause.ask_master)
    }
    println("================================================================ [2]")
  }
  
  @Test
  fun testAFewShards() {
    val shard_config = createShardConfig("testAFewShards")
    val shard_data = createShardCluster(shard_config, "sc5")
    val client = shard_data.client

    println("================================================================ [1]")
    val first = measureTimeMillis {
      (1..20)
          .map {
            val shard_id = "shard-$it"
            shard_id to client.ask<TestShardProtocol.HelloThereBack>(shard_id, TestShardProtocol.HelloThere("client"))
          }
          .forEach {
            Assert.assertEquals("hello client from ${it.first}", it.second.join().greetings)
          }
    }
    println("first: $first ms")
    println("================================================================ [2]")
    val second = measureTimeMillis {
      (1..20)
          .map {
            val shard_id = "shard-$it"
            shard_id to client.ask<TestShardProtocol.HelloThereBack>(shard_id, TestShardProtocol.HelloThere("client"))
          }
          .forEach {
            Assert.assertEquals("hello client from ${it.first}", it.second.join().greetings)
          }
    }
    println("second: $second ms")
    println("================================================================ [3]")
  }
  
  @Test
  @Ignore("this prints out a lot of information")
  fun testALotOfShards() {
    val shard_config = createShardConfig("testALotOfShards")
    val shard_data = createShardCluster(shard_config, "sc6", 5, 1000, start_wait_timeout = 20_000)
    val client = shard_data.client

    println("================================================================ [1]")
    val first = measureTimeMillis {
      (1..1000)
          .map {
            val shard_id = "shard-$it"
            shard_id to client.ask<TestShardProtocol.HelloThereBack>(shard_id, TestShardProtocol.HelloThere("client"))
          }
          .forEach {
            Assert.assertEquals("hello client from ${it.first}", it.second.join().greetings)
          }
    }
    println("================================================================ [2]")
    val second = measureTimeMillis {
      (1..1000)
          .map {
            val shard_id = "shard-$it"
            shard_id to client.ask<TestShardProtocol.HelloThereBack>(shard_id, TestShardProtocol.HelloThere("client"))
          }
          .forEach {
            Assert.assertEquals("hello client from ${it.first}", it.second.join().greetings)
          }
    }
    println("================================================================ [3]")
    println("first: $first ms")
    println("second: $second ms")
    Thread.sleep(5000)
  }

  @Test
  fun testALotOfClients() {
    val shard_config = createShardConfig("testALotOfClients")
    val shard_data = createShardCluster(shard_config, "sc7")

    println("================================================================ [1]")
    (1..20).forEach { client_it ->
      println("client_it: $client_it")
      val client = register(shard_factory.client(shard_config, shard_data.pubsub, getMarker("sc7-client-$client_it")))
      client.start_promise.withTimeout(1000L).join()
      (0..99).forEach {
        val shard_id = "shard-${it % 20 + 1}"
        val result = client.ask<TestShardProtocol.HelloThereBack>(shard_id, TestShardProtocol.HelloThere("world"))
        val check = result.withTimeout(3000).join()
        Assert.assertNotNull(check)
        Assert.assertEquals("hello world from $shard_id", check.greetings) 
      }
      client.shutdown().withTimeout(1000L).join()
    }
    println("================================================================ [2]")
  }

}
