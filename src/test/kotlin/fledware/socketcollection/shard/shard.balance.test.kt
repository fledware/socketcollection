package fledware.socketcollection.shard

import fledware.socketcollection.CleanHelper
import fledware.socketcollection.LOCALHOST
import fledware.socketcollection.Promise
import fledware.socketcollection.dump_socket
import fledware.socketcollection.nextPort
import fledware.socketcollection.pubsub.pubsub_factory
import fledware.socketcollection.waitUntil
import fledware.socketcollection.withTimeout
import kotlinx.coroutines.experimental.future.future
import org.junit.Before
import org.junit.Test
import org.slf4j.MarkerFactory.getMarker

class ShardBalanceTest : CleanHelper() {
  private val pubsub = pubsub_factory.localBus()
  @Before
  fun before() {
    pubsub.clearSubscriptions()
  }

  @Test
  fun testShardsDistribute() {
    val kill = Promise<Unit>()
    val shard_config = createShardConfig("testShardsDistribute")
    val master = register(shard_factory.master(shard_config, pubsub, createShardSeed(), marker = getMarker("sb1-master")))
    master.start_promise.join()

    val node_1 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb1-node1")))
    node_1.start_promise.join()
    val node_2 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb1-node2")))
    node_2.start_promise.join()

    waitUntil(timeout = 5_000) { isNodesBalanced(20, shard_config.balancing_threshold, true) }
  }

  @Test
  fun testLateNodeLeave() {
    val kill = Promise<Unit>()
    val shard_config = createShardConfig("testShardsDistribute")
    val master = register(shard_factory.master(shard_config, pubsub, createShardSeed(), marker = getMarker("sb2-master")))
    master.start_promise.join()

    val node_1 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb2-node1")))
    node_1.start_promise.join()
    val node_2 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb2-node2")))
    node_2.start_promise.join()

    waitUntil(timeout = 5_000) { isNodesBalanced(20, shard_config.balancing_threshold, true) }

    node_1.shutdown().join()
    waitUntil(timeout = 5_000) { isNodesBalanced(20, shard_config.balancing_threshold, true) }
  }

  @Test
  fun testLateNodeJoin() {
    val kill = Promise<Unit>()
    val shard_config = createShardConfig("testLateNodeJoin")
    val master = register(shard_factory.master(shard_config, pubsub, createShardSeed(), marker = getMarker("sb3-master")))
    master.start_promise.join()

    val node_1 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb3-node1")))
    node_1.start_promise.join()
    waitUntil(timeout = 5_000) { isNodesBalanced(20, shard_config.balancing_threshold, true) }

    val node_2 = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) }, kill, marker = getMarker("sb3-node2")))
    node_2.start_promise.join()
    waitUntil(timeout = 5_000) { isNodesBalanced(20, shard_config.balancing_threshold, true) }
  }

  @Test
  fun testNodeLeavesAndEnters5Times() {
    val shard_config = createShardConfig("testNodeLeavesAndEnters5Times")
    val shard_data = createShardCluster(shard_config, "sb4", shard_count = 40)

    (10..14).forEach {
      println("============================================================= [$it]")

      val node_bad = register(shard_factory.node(shard_config, LOCALHOST, nextPort(), { TestShard(it) },
                                                 shard_data.kill, marker = getMarker("sb4-node-$it")))
      node_bad.start().join()
      waitUntil(timeout = 5_000) { isNodesBalanced(40, shard_config.balancing_threshold, true) }
      isNodesBalanced(40, shard_config.balancing_threshold, true)
      node_bad.shutdown().join()
      waitUntil(timeout = 5_000) { isNodesBalanced(40, shard_config.balancing_threshold, true) }
      isNodesBalanced(40, shard_config.balancing_threshold, true)
    }
  }

  @Test
  fun testNodeHasInvalidExtraShard() {
    val shard_config = createShardConfig("testNodeHasInvalidExtraShard").copy()
    val shard_data = createShardCluster(shard_config, "sb5")

    future {
      shard_data.nodes[0].masterHandleInbound(dump_socket, ShardProtocol.ShardStart("shard-extra"), false)
    }.join()

    waitUntil(timeout = 10_000, sleep = 500) { isNodesBalanced(20, shard_config.balancing_threshold, true) }
  }

  @Test
  fun testNodeHasValidExtraShard() {
    val shard_config = createShardConfig("testNodeHasValidExtraShard")
    val shard_data = createShardCluster(shard_config, "sb6")

    future {
      shard_data.nodes.forEach {
        it.masterHandleInbound(dump_socket, ShardProtocol.ShardStart("shard-3"), false)
      }
    }.join()

    waitUntil(timeout = 10_000, sleep = 500) { isNodesBalanced(20, shard_config.balancing_threshold, true) }
  }

  @Test
  fun testShardDataInvalid_StatesNoNode() {
    val shard_config = createShardConfig("testShardDataInvalid_StatesNoNode")
    val shard_data = createShardCluster(shard_config, "sb7")

    val shard_messing = shard_data.master.shards.values.last()
    shard_messing.node_on = null

    waitUntil(timeout = 10_000, sleep = 500) {
      isNodesBalanced(20, shard_config.balancing_threshold, true) && shard_messing.node_on != null
    }
  }

  @Test
  fun testShardDataInvalid_StatesOnInvalidNode() {
    val shard_config = createShardConfig("testShardDataInvalid_StatesNoNode")
    val shard_data = createShardCluster(shard_config, "sb8")

    val shard_messing = shard_data.master.shards.values.last()
    shard_messing.node_on = "lalalala"

    waitUntil(timeout = 10_000, sleep = 500) {
      isNodesBalanced(20, shard_config.balancing_threshold, true) && shard_messing.node_on != "lalalala"
    }
  }

  @Test
  fun testShardDataInvalid_GhostShardOnNodeData() {
    val shard_config = createShardConfig("testShardDataInvalid_GhostShardOnNodeData")
    val shard_data = createShardCluster(shard_config, "sb9")
    
    val node = shard_data.master.shard_collection.sockets.values.first().data.node_data!!
    node.shards.add("some-invalid-shard")

    waitUntil(timeout = 10_000, sleep = 500) {
      isNodesBalanced(20, shard_config.balancing_threshold, true)
    }
  }

  @Test
  fun testShardDataInvalid_StateInvalidNodeAndDuplicate() {
    val shard_config = createShardConfig("testShardDataInvalid_StatesNoNode")
    val shard_data = createShardCluster(shard_config, "sb10")

    val shard_messing = shard_data.master.shards.values.last()
    shard_messing.node_on = "lalalala"

    future {
      shard_data.nodes.forEach {
        it.masterHandleInbound(dump_socket, ShardProtocol.ShardStart(shard_messing.identifier), false)
      }
    }.join()

    waitUntil(timeout = 10_000, sleep = 500) {
      isNodesBalanced(20, shard_config.balancing_threshold, true) && shard_messing.node_on != "lalalala"
    }
  }

  @Test
  fun testMasterLeaveAndComeback() {
    val shard_config = createShardConfig("testMasterLeaveAndComeback")
    val shard_data = createShardCluster(shard_config, "sb11")

    println("================================================================ [0]")
    shard_data.master.shutdown().withTimeout(5000).join()
    Thread.sleep(5000)
    println("================================================================ [1]")
    
    val new_master = register(shard_factory.master(shard_config, pubsub, createShardSeed(20), marker = getMarker("sb11-2-master")))
    println("================================================================ [2]")

    waitUntil(timeout = 10_000, sleep = 500) {
      isNodesBalanced(20, shard_config.balancing_threshold, true) && new_master.start_promise.isDone
    }
    println("================================================================ [3]")
  }

}
