package fledware.socketcollection

import fledware.socketcollection.factory.collectionAsConfigured
import fledware.socketcollection.factory.collectionAsServer
import fledware.socketcollection.factory.collectionAsServerRouter
import fledware.socketcollection.factory.socketCreateStandalone
import fledware.socketcollection.internal.currentTime
import org.junit.Assert
import org.junit.Test
import org.slf4j.MarkerFactory
import java.util.concurrent.atomic.AtomicInteger


class RoutingTest : CleanHelper() {
  @Test
  fun testSimpleClientMessage() {
    val server = register(collectionAsServerRouter(nextPort()) { _, message, _ -> "hello $message!" })
    val client = register(socketCreateStandalone(LOCALHOST, server.bound_ports.first()))
    startAllServices()
    client.waitForState().join()

    val result = client.ask<String>("world").withTimeout(6000)
    Assert.assertEquals("hello world!", result.join())

    Assert.assertEquals(1, server.active.size)
    Assert.assertEquals(SocketState.NORMAL, server.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, client.socket_state)
  }

  @Test
  fun testSimpleServerMessage() {
    val server = register(collectionAsServerRouter(nextPort()))
    register(socketCreateStandalone(LOCALHOST, server.bound_ports.first()) { _, message, _ -> "hello $message!" })
    startAllServices()

    val result = server.ask<String>("world").withTimeout(6000)
    Assert.assertEquals("hello world!", result.join())
  }
  
  @Test
  fun testALotOfMessages() {
    val server = register(collectionAsServerRouter(nextPort()) { _, message, _ -> "hello $message!" })
    val client = register(socketCreateStandalone(LOCALHOST, server.bound_ports.first()))
    startAllServices()
    client.waitForState().join()

    val result = client.ask<String>("world").withTimeout(6000)
    Assert.assertEquals("hello world!", result.join())
    
    val count = 50_000
    val start = currentTime()
    (1..count)
        .map { it to client.ask<String>("world$it") }
        .forEach { Assert.assertEquals("hello world${it.first}!", it.second.join()) }
    println("$count messages in ${currentTime() - start} ms")
  }

  @Test
  fun testALotOfMessagesFromMultiple() {
    val server = register(collectionAsServerRouter(nextPort()) { _, message, _ -> "hello $message!" })
    val clients = (1..5).map { register(socketCreateStandalone(LOCALHOST, server.bound_ports.first())) }
    startAllServices()
    clients.forEach { it.waitForState().withTimeout(2000).join() }

    val start = currentTime()
    val count = 50_000
    (0..count)
        .map { it to clients[it % clients.size].ask<String>("world$it") }
        .forEach { Assert.assertEquals("hello world${it.first}!", it.second.join()) }
    println("$count messages in ${currentTime() - start} ms")
  }
  
  @Test
  fun testALotOfMessagesWithManyClients() {
    val servers = (1..5).map { register(collectionAsServerRouter(nextPort()) { _, message, _ -> "hello $message!" }) }
    val client = register(collectionAsConfigured())
    startAllServices()
    servers.forEach { client.connect(LOCALHOST, it.bound_ports.first()).waitForState().withTimeout(1000L).join() }

    val count = 50_000
    val start = currentTime()
    (1..count)
        .map { it to client.ask<String>("world$it") }
        .forEach { Assert.assertEquals("hello world${it.first}!", it.second.join()) }
    println("$count messages in ${currentTime() - start} ms")
  }

  @Test
  fun testRoutingMessages() {
    val total = AtomicInteger(0)
    val handler: InboundHandler = { socket, message, _ ->
      socket.data.compute("count") { _, value -> if (value == null) 1 else value as Int + 1 }
      println("[${socket.identity}] $message -> ${total.incrementAndGet()}")
      "hello $message!"
    }
    val server1 = register(collectionAsServer(nextPort(), "server1", handler = handler))
    val server2 = register(collectionAsServer(nextPort(), "server2", handler = handler))
    val server3 = register(collectionAsServer(nextPort(), "server3", handler = handler))
    startAllServices()

    val client = register(collectionAsConfigured())
    client.connect(LOCALHOST, server1.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(6000).join()
    client.connect(LOCALHOST, server2.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(6000).join()
    client.connect(LOCALHOST, server3.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(6000).join()

    (1..30).map { client.ask<String>("world") }.forEach { it.join() }

    Assert.assertEquals(1, server1.active.size)
    Assert.assertEquals(1, server2.active.size)
    Assert.assertEquals(1, server3.active.size)
    Assert.assertEquals(SocketState.NORMAL, server1.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, server2.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, server3.active[0].socket_state)
    Assert.assertEquals(3, client.active.size)
    Assert.assertEquals(SocketState.NORMAL, client.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, client.active[1].socket_state)
    Assert.assertEquals(SocketState.NORMAL, client.active[2].socket_state)

    Assert.assertEquals(30, total.get())
    Assert.assertEquals(10, server1.active[0].data["count"])
    Assert.assertEquals(10, server2.active[0].data["count"])
    Assert.assertEquals(10, server3.active[0].data["count"])
  }

  @Test
  fun testStandaloneReceivedClose() {
    val collection = factory.collectionAsServer(nextPort(),
                                                config = SocketCollectionConfig(
                                                    socket_message_buffer = 1000,
                                                    socket_force_reconnect = false,
                                                    collection_message_buffer = -1,
                                                    collection_remove_on_down = false
                                                ),
                                                marker = MarkerFactory.getMarker("coll"),
                                                handler = { _, message, _ ->
                                                  println("collection message: $message")
                                                  "hello $message"
                                                })

    val standalone = factory.socketCreateStandalone(LOCALHOST,
                                                    collection.bound_ports.first(),
                                                    marker = MarkerFactory.getMarker("alone"),
                                                    handler = { _, message, _ ->
                                                      println("standalone message: $message")
                                                      "whatever $message"
                                                    })
    register(collection)
    register(standalone)
    startAllServices()

    standalone.waitForState().withTimeout(1000L).join()
    Assert.assertEquals(SocketState.NORMAL, standalone.socket_state)
    val check_2 = standalone.ask<String>("other1").withTimeout(1000L).join()
    Assert.assertEquals("hello other1", check_2)

    Assert.assertEquals(1, collection.sockets.size)
    val check_1 = collection.sockets.values.first().ask<String>("other").withTimeout(1000L).join()
    Assert.assertEquals("whatever other", check_1)

    collection.shutdown().join()
    
    println(standalone.socket_state)
    standalone.waitForState(SocketState.DOWN_FINDING).withTimeout(2000L).join()
  }

  @Test
  fun testRemoteSocketKilled() {
    val collection = factory.collectionAsServer(nextPort(),
                                                config = SocketCollectionConfig(
                                                    socket_message_buffer = 1000,
                                                    socket_force_reconnect = false,
                                                    collection_message_buffer = -1,
                                                    collection_remove_on_down = false
                                                ),
                                                marker = MarkerFactory.getMarker("coll"),
                                                handler = { _, message, _ ->
                                                  println("collection message: $message")
                                                  "hello $message"
                                                })

    val standalone = factory.socketCreateStandalone(LOCALHOST,
                                                    collection.bound_ports.first(),
                                                    marker = MarkerFactory.getMarker("alone"),
                                                    handler = { _, message, _ ->
                                                      println("standalone message: $message")
                                                      "whatever $message"
                                                    })
    register(collection)
    register(standalone)
    startAllServices()

    standalone.waitForState().withTimeout(1000L).join()
    Assert.assertEquals(SocketState.NORMAL, standalone.socket_state)
    val check_2 = standalone.ask<String>("other1").withTimeout(1000L).join()
    Assert.assertEquals("hello other1", check_2)

    Assert.assertEquals(1, collection.sockets.size)
    val check_1 = collection.sockets.values.first().ask<String>("other").withTimeout(1000L).join()
    Assert.assertEquals("whatever other", check_1)

    collection.sockets.values.first().shutdown()

    collection.shutdown().join()

    println(standalone.socket_state)
    standalone.waitForState(SocketState.DOWN_FINDING).withTimeout(2000L).join()
  }
  
  @Test
  fun testSocketActuallyRetriesAsk() {
    val collection = factory.collectionAsServer(nextPort(),
                                                handler = { socket, message, _ ->
                                                  // basically, just wait forever on the first
                                                  // call from all sockets
                                                  if (socket.data.containsKey("ok")) {
                                                    return@collectionAsServer "hello $message"
                                                  }
                                                  else {
                                                    socket.data["ok"] = true
                                                    return@collectionAsServer NoResponse
                                                  }
                                                })
    val standalone = factory.socketCreateStandalone(LOCALHOST, collection.bound_ports.first())
    register(collection)
    register(standalone)
    startAllServices()
    
    println(standalone.config)
    val result = standalone.ask<String>("people!").join()
    Assert.assertEquals("hello people!", result)
  }

  @Test
  fun testSocketActuallyRetriesAsk2() {
    val collection = factory.collectionAsServer(nextPort(),
                                                handler = { socket, message, _ ->
                                                  // basically, just wait forever on the first
                                                  // call from all sockets
                                                  if (socket.data.containsKey("ok")) {
                                                    return@collectionAsServer "hello $message"
                                                  }
                                                  else {
                                                    socket.data["ok"] = true
                                                    return@collectionAsServer NoResponse
                                                  }
                                                })
    val client = factory.collectionAsConfigured()
    register(collection)
    register(client)
    startAllServices()
    client.connect(LOCALHOST, collection.bound_ports.first()).waitForState().withTimeout(1000).join()
    client.connect(LOCALHOST, collection.bound_ports.first()).waitForState().withTimeout(1000).join()
    client.connect(LOCALHOST, collection.bound_ports.first()).waitForState().withTimeout(1000).join()
    client.connect(LOCALHOST, collection.bound_ports.first()).waitForState().withTimeout(1000).join()
    
    (1..1000).map {
      it to client.ask<String>("people $it!")
    }.forEach {
      Assert.assertEquals("hello people ${it.first}!", it.second.join())
    }
  }

  @Test
  fun testConnectALot() {
    val collection = register(factory.collectionAsServer(nextPort()) { _, message, _ -> "hello $message" })
    startAllServices()
    
    (1..20).forEach {
      println("==================================== [$it]")
      Assert.assertEquals(0, collection.active.size)
      
      val standalone = factory.socketCreateStandalone(LOCALHOST, collection.bound_ports.first())
      standalone.waitForState().withTimeout(3000).join()
      waitUntil { collection.active.size == 1 }
      
      val check = standalone.ask<String>("other $it").withTimeout(1000L).join()
      Assert.assertEquals("hello other $it", check)

      standalone.shutdown().withTimeout(1000).join()
      waitUntil { collection.active.isEmpty() }
    }
  }
}
