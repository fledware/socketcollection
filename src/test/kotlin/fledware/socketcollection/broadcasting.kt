package fledware.socketcollection

import fledware.socketcollection.factory.collectionAsConfigured
import fledware.socketcollection.factory.collectionAsServer
import fledware.socketcollection.factory.collectionAsServerRouter
import org.junit.Assert
import org.junit.Test


class BroadcastTest : CleanHelper() {
  @Test
  fun testSimpleBroadcastClient() {
    val handler: InboundHandler = { socket, message, _ ->
      socket.data.compute("count") { _, value -> if (value == null) 1 else value as Int + 1 }
      println("[${socket.identity}] $message")
      null
    }
    val server1 = register(collectionAsServer(nextPort(), "server1", handler = handler))
    val server2 = register(collectionAsServer(nextPort(), "server2", handler = handler))
    val server3 = register(collectionAsServer(nextPort(), "server3", handler = handler))
    startAllServices()

    val client = register(collectionAsConfigured())
    client.connect(LOCALHOST, server1.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(1000).join()
    client.connect(LOCALHOST, server2.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(1000).join()
    client.connect(LOCALHOST, server3.bound_ports.first()).waitForState(SocketState.NORMAL).withTimeout(1000).join()

    val ask1 = client.broadcastAsk<Unit>("message1")
    Assert.assertEquals(3, ask1.size)
    ask1.forEach { it.withTimeout(1000).join() }
    val ask2 = client.broadcastAsk<Unit>("message2")
    Assert.assertEquals(3, ask2.size)
    ask2.forEach { it.withTimeout(1000).join() }

    Assert.assertEquals(2, server1.sockets.values.first().data["count"])
    Assert.assertEquals(2, server2.sockets.values.first().data["count"])
    Assert.assertEquals(2, server3.sockets.values.first().data["count"])
  }

  @Test
  fun testSimpleBroadcastServer() {
    val handler: InboundHandler = { socket, message, _ ->
      socket.data.compute("count") { _, value -> if (value == null) 1 else value as Int + 1 }
      println("[${socket.identity}] $message")
      null
    }
    val server = register(collectionAsServerRouter(nextPort()))
    startAllServices()

    val client1 = register(collectionAsConfigured(handler = handler))
    val client2 = register(collectionAsConfigured(handler = handler))
    val client3 = register(collectionAsConfigured(handler = handler))
    client1.connect(LOCALHOST, server.bound_ports.first()).waitForState().withTimeout(1000).join()
    client2.connect(LOCALHOST, server.bound_ports.first()).waitForState().withTimeout(1000).join()
    client3.connect(LOCALHOST, server.bound_ports.first()).waitForState().withTimeout(1000).join()

    Assert.assertEquals(1, client1.active.size)
    Assert.assertEquals(1, client2.active.size)
    Assert.assertEquals(1, client3.active.size)
    Assert.assertEquals(SocketState.NORMAL, client1.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, client2.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, client3.active[0].socket_state)
    Assert.assertEquals(3, server.active.size)
    Assert.assertEquals(SocketState.NORMAL, server.active[0].socket_state)
    Assert.assertEquals(SocketState.NORMAL, server.active[1].socket_state)
    Assert.assertEquals(SocketState.NORMAL, server.active[2].socket_state)

    server.broadcastAsk<Unit>("message1").forEach { it.withTimeout(1000).join() }
    server.broadcastAsk<Unit>("message2").forEach { it.withTimeout(1000).join() }

    Assert.assertEquals(2, client1.sockets.values.first().data["count"])
    Assert.assertEquals(2, client2.sockets.values.first().data["count"])
    Assert.assertEquals(2, client3.sockets.values.first().data["count"])
  }
}
