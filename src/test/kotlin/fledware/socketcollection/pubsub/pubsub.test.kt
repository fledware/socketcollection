package fledware.socketcollection.pubsub

import fledware.socketcollection.CleanHelper
import fledware.socketcollection.LOCALHOST
import fledware.socketcollection.nextPort
import fledware.socketcollection.waitUntil
import fledware.socketcollection.withTimeout
import kotlinx.coroutines.experimental.future.future
import org.junit.Assert
import org.junit.Test
import org.slf4j.MarkerFactory.getMarker


fun MutableMap<String, Any>.increment(key: String) {
  compute(key) { _, value -> if (value == null) 1 else value as Int + 1 }
}

class PubSubTest: CleanHelper() {
  companion object {
    private fun handler(client: PubSubClientImpl): EventHandler {
      return { topics, event ->
        println("${client.name}: $event")
        client.server.data.increment("count")
        topics.forEach { topic ->
          client.server.data.increment(topic)
        }
      }
    }
  }

  @Test
  fun testHappyPath() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("ps1-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps1-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps1-client2")))
    val client3 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps1-client3")))
    startAllServices()

    println("====================================================== [0]")
    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client3.server.waitForState().withTimeout(1000).join()

    println("====================================================== [1]")
    client1.subscribe(setOf("testing"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing"), handler(client2)).withTimeout(1000).join()
    client3.subscribe(setOf("testing"), handler(client3)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.contains("testing") }
    waitUntil { client2.cluster_listens.contains("testing") }
    waitUntil { client3.cluster_listens.contains("testing") }
    Thread.sleep(100)

    println("====================================================== [2]")
    client2.publish(setOf("testing"), "hello world!")
    Thread.sleep(100)
    Assert.assertEquals(1, client1.server.data["count"])
    Assert.assertEquals(1, client2.server.data["count"])
    Assert.assertEquals(1, client3.server.data["count"])
  }

  @Test
  fun testLateJoin() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("ps2-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps2-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps2-client2")))
    startAllServices()

    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client1.subscribe(setOf("testing"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing"), handler(client2)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.contains("testing") }
    waitUntil { client2.cluster_listens.contains("testing") }

    val client3 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps2-client3")))
    future { client3.start() }.join()
    client3.server.waitForState().withTimeout(1000).join()
    waitUntil { client3.cluster_listens.contains("testing") }

    client1.publish(setOf("testing"), "hello world!")
    client2.publish(setOf("testing"), "hello world!")
    client3.publish(setOf("testing"), "hello world!")
    Thread.sleep(100)

    waitUntil {
      val count1 = client1.server.data["count"]
      val count2 = client2.server.data["count"]
      println("count1: $count1 .... count2: $count2")
      count1 == 3 && count2 == 3
    }

    Assert.assertEquals(null, client3.server.data["count"])
  }

  @Test
  fun testMultipleTopics() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("ps3-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps3-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps3-client2")))
    val client3 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("ps3-client3")))
    startAllServices()

    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client3.server.waitForState().withTimeout(1000).join()
    client1.subscribe(setOf("testing", "testing-123"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing", "testing-234"), handler(client2)).withTimeout(1000).join()
    client3.subscribe(setOf("testing", "testing-345"), handler(client3)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.contains("testing") }
    waitUntil { client2.cluster_listens.contains("testing") }
    waitUntil { client3.cluster_listens.contains("testing") }
    Thread.sleep(100)

    client3.server.waitForState().withTimeout(1000).join()
    Thread.sleep(100)

    client1.publish(setOf("testing", "testing-123"), "hello world!")
    client2.publish(setOf("testing", "testing-234"), "hello world!")
    client3.publish(setOf("testing", "testing-345"), "hello world!")
    Thread.sleep(100)

    Assert.assertEquals(3, client1.server.data["count"])
    Assert.assertEquals(3, client2.server.data["count"])
    Assert.assertEquals(3, client3.server.data["count"])
  }

  @Test
  fun testDeeperTopics() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("pc4-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc4-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc4-client2")))
    val client3 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc4-client3")))
    val client4 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc4-client4")))
    startAllServices()

    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client3.server.waitForState().withTimeout(1000).join()
    client4.server.waitForState().withTimeout(1000).join()
    client1.subscribe(setOf("testing-123"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing-234"), handler(client2)).withTimeout(1000).join()
    client3.subscribe(setOf("testing-345"), handler(client3)).withTimeout(1000).join()
    client4.subscribe(setOf("testing"), handler(client4)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.contains("testing-123") }
    waitUntil { client2.cluster_listens.contains("testing-234") }
    waitUntil { client3.cluster_listens.contains("testing-345") }
    waitUntil { client3.cluster_listens.contains("testing") }
    Thread.sleep(100)

    client3.server.waitForState().withTimeout(1000).join()
    Thread.sleep(100)

    client1.publish(setOf("testing", "testing-123"), "hello world!")
    client2.publish(setOf("testing", "testing-234"), "hello world!")
    client3.publish(setOf("testing", "testing-345"), "hello world!")
    client4.publish(setOf("testing"), "hello world!")
    Thread.sleep(100)

    Assert.assertEquals(1, client1.server.data["count"])
    Assert.assertEquals(1, client2.server.data["count"])
    Assert.assertEquals(1, client3.server.data["count"])
    Assert.assertEquals(4, client4.server.data["count"])
  }

  @Test
  fun testDifferentSubs() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("pc5-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc5-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc5-client2")))
    startAllServices()

    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client1.subscribe(setOf("testing", "some-topic"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing", "some-other-topic"), handler(client2)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.containsAll(setOf("testing", "some-topic", "some-other-topic")) }
    waitUntil { client2.cluster_listens.containsAll(setOf("testing", "some-topic", "some-other-topic")) }
    Thread.sleep(100)

    client1.publish(setOf("testing", "some-non-topic"), "hello world!")
    client2.publish(setOf("testing", "some-non-topic"), "hello world!")
    client1.publish(setOf("some-topic"), "hello world!")
    client2.publish(setOf("some-topic"), "hello world!")
    client1.publish(setOf("some-other-topic"), "hello world!")
    client2.publish(setOf("some-other-topic"), "hello world!")
    Thread.sleep(100)

    Assert.assertEquals(4, client1.server.data["count"])
    Assert.assertEquals(4, client2.server.data["count"])
  }

  @Test
  fun testMultiplePubTopics() {
    val server = register(pubsub_factory.server(nextPort(), marker = getMarker("pc6-server")))
    val client1 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc5-client1")))
    val client2 = register(pubsub_factory.client(LOCALHOST, server.bind, marker = getMarker("pc5-client2")))
    startAllServices()

    client1.server.waitForState().withTimeout(1000).join()
    client2.server.waitForState().withTimeout(1000).join()
    client1.subscribe(setOf("testing", "some-topic"), handler(client1)).withTimeout(1000).join()
    client2.subscribe(setOf("testing", "some-other-topic"), handler(client2)).withTimeout(1000).join()
    waitUntil { client1.cluster_listens.containsAll(setOf("testing", "some-topic", "some-other-topic")) }
    waitUntil { client2.cluster_listens.containsAll(setOf("testing", "some-topic", "some-other-topic")) }
    Thread.sleep(100)

    client1.publish(setOf("testing", "some-non-topic"), "hello world!")
    client2.publish(setOf("testing", "some-non-topic"), "hello world!")
    client1.publish(setOf("testing", "some-topic"), "hello world!")
    client2.publish(setOf("testing", "some-topic"), "hello world!")
    client1.publish(setOf("testing", "some-other-topic"), "hello world!")
    client2.publish(setOf("testing", "some-other-topic"), "hello world!")
    Thread.sleep(100)

    Assert.assertEquals(6, client1.server.data["count"])
    Assert.assertEquals(6, client2.server.data["count"])
  }
}
